<?php
namespace Netfed\CcEvents\Mvc\View;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Extbase\Mvc\View\JsonView as ExtbaseJsonView;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class JsonView
 *
 */
class JsonView extends ExtbaseJsonView
{
    /**
     * @var array
     */
    protected $configuration = [];

    /**
     * Transforming ObjectStorages to Arrays for the JSON view
     *
     * @param mixed $value
     * @param array $configuration
     * @return array
     */
    protected function transformValue($value, array $configuration)
    {
        if ($value instanceof ObjectStorage) {
            $value = $value->toArray();
        }
        $value = parent::transformValue($value, $configuration);
        return is_string($value) ? utf8_encode($value) : $value;
    }

    /**
     * Transforms the value view variable to a serializable
     * array representation using a YAML view configuration and JSON encodes
     * the result.
     *
     * @return string The JSON encoded variables
     * @api
     */
    public function render()
    {
        $response = $this->controllerContext->getResponse();
        if ($response instanceof WebResponse) {
            // @todo Ticket: #63643 This should be solved differently once request/response model is available for TSFE.
            if (!empty($GLOBALS['TSFE']) && $GLOBALS['TSFE'] instanceof TypoScriptFrontendController) {
                /** @var TypoScriptFrontendController $typoScriptFrontendController */
                $typoScriptFrontendController = $GLOBALS['TSFE'];
                if (empty($typoScriptFrontendController->config['config']['disableCharsetHeader'])) {
                    // If the charset header is *not* disabled in configuration,
                    // TypoScriptFrontendController will send the header later with the Content-Type which we set here.
                    $typoScriptFrontendController->setContentType('application/json');
                } else {
                    // Although the charset header is disabled in configuration, we *must* send a Content-Type header here.
                    // Content-Type headers optionally carry charset information at the same time.
                    // Since we have the information about the charset, there is no reason to not include the charset information although disabled in TypoScript.
                    $response->setHeader('Content-Type', 'application/json; charset=utf-8');
                }
            } else {
                $response->setHeader('Content-Type', 'application/json');
            }
        }
        $propertiesToRender = $this->renderArray();
//        DebuggerUtility::var_dump($propertiesToRender);die;
        $json = $this->utf8_converter($propertiesToRender);
//        printf($json);die;
        $json = json_encode($json);
//        $propertiesToRender = unserialize(base64_decode(serialize($propertiesToRender[1])));
//        $json =json_encode($propertiesToRender, JSON_UNESCAPED_UNICODE);
        if (!$json) {
            echo json_last_error_msg(); die;
        }
        return $json;
//        return json_encode($propertiesToRender, 0, 8096);
    }

    /**
     * @param $array
     * @return mixed
     */
    function utf8_converter($array)
    {
        array_walk_recursive($array, function(&$item, $key){
            if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

}
