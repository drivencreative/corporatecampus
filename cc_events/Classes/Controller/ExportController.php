<?php
namespace Netfed\CcEvents\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * ExportController
 */
class ExportController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * eventRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\RegistrationRepository
     * @inject
     */
    protected $registrationRepository = null;

    /**
     * action index
     *
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('registrations', $this->getRegistrations());
    }

    /**
     * action process
     *
     * @return void
     */
    public function processAction()
    {
        $registrations = $this->getRegistrations();

        /** @var $excel \PHPExcel **/
        $excel = $this->objectManager->get(\PHPExcel::class);
        $excel->getProperties()->setTitle('User registrations');

        /** @var $sheet \PHPExcel_Worksheet **/
        $sheet = $excel->getSheet(0);
        $sheet->setTitle('Data');

        $sheet->getDefaultStyle()->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('a1:o1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('cecece');
        $sheet->setAutoFilter($sheet->calculateWorksheetDimension());

        $sheet->setCellValueByColumnAndRow(0, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.firstName', $this->extensionName)); // Nachname  (last name)
        $sheet->setCellValueByColumnAndRow(1, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.lastName', $this->extensionName)); // 'Vorname (surname)'
        $sheet->setCellValueByColumnAndRow(2, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.anrede', $this->extensionName)); // 'Anrede (salutation)'
        $sheet->setCellValueByColumnAndRow(3, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.title', $this->extensionName)); // 'Titel (title)'
        $sheet->setCellValueByColumnAndRow(4, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.company', $this->extensionName)); // 'Unternehmen (company)'
        $sheet->setCellValueByColumnAndRow(5, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.function', $this->extensionName)); // 'Funktion (function)'
        $sheet->setCellValueByColumnAndRow(6, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.address', $this->extensionName)); // 'Straße (street)'
        $sheet->setCellValueByColumnAndRow(7, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.zip', $this->extensionName)); // 'PLZ (postal code)'
        $sheet->setCellValueByColumnAndRow(8, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.city', $this->extensionName)); // 'Stadt (city)'
        $sheet->setCellValueByColumnAndRow(9, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.telephone', $this->extensionName)); // 'Telefon (phone)'
        $sheet->setCellValueByColumnAndRow(10, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.email', $this->extensionName)); // 'E-Mail Adresse'
//        $sheet->setCellValueByColumnAndRow(11, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration', $this->extensionName)); // 'Hierarchie-Ebene (hierarchy level)'
//        $sheet->setCellValueByColumnAndRow(12, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration', $this->extensionName)); // 'Unternehmen Abkürzung (company abbreveation)'
        $sheet->setCellValueByColumnAndRow(13, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.description', $this->extensionName)); // 'Anmerkung (remark)'
        $sheet->setCellValueByColumnAndRow(14, 1, LocalizationUtility::translate('tx_ccevents_domain_model_registration.terms', $this->extensionName)); // 'Einverständnis-erklärungen Internetseite (declaration of consent)'

        $column = 15;
        $position = [];
        foreach ($this->extractByKey($registrations, 'event') as $event) {
            $sheet->setCellValueByColumnAndRow($column, 1, $event->getTitle());
            $position[$event->getUid()] = $column;
            $column++;
        }

        $sheet->setCellValueByColumnAndRow($column, 1, 'SUMME');
        $sheet->getStyle('P1:' . $sheet->getColumnDimensionByColumn($column)->getColumnIndex() . '1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('faffcc');
        $sheet->getStyle($sheet->getColumnDimensionByColumn($column)->getColumnIndex() . '1')->getFill()->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffbb66');

        $row = 2;
        foreach ($this->groupElements($registrations, 'firstName') as $users) {
            /** @var $user \Netfed\CcEvents\Domain\Model\Registration **/
            $user = current($users);

            $sheet->setCellValueByColumnAndRow(0, $row, $user->getFirstName());
            $sheet->setCellValueByColumnAndRow(1, $row, $user->getLastName());
            $sheet->setCellValueByColumnAndRow(2, $row, $user->getAnrede());
            $sheet->setCellValueByColumnAndRow(3, $row, $user->getTitle());
            $sheet->setCellValueByColumnAndRow(4, $row, $user->getCompany());
            $sheet->setCellValueByColumnAndRow(5, $row, $user->getFunction());
            $sheet->setCellValueByColumnAndRow(6, $row, $user->getAddress());
            $sheet->setCellValueByColumnAndRow(7, $row, $user->getZip());
            $sheet->setCellValueByColumnAndRow(8, $row, $user->getCity());
            $sheet->setCellValueByColumnAndRow(9, $row, $user->getTelephone());
            $sheet->setCellValueByColumnAndRow(10, $row, $user->getEmail());
//            $sheet->setCellValueByColumnAndRow(11, $row, $user->get);
//            $sheet->setCellValueByColumnAndRow(12, $row, $user->get);
            $sheet->setCellValueByColumnAndRow(13, $row, $user->getDescription());
            $sheet->setCellValueByColumnAndRow(14, $row, $user->getTerms());

            foreach ($users as $usr) {
                if ($usr->getEvent()) {
                    $sheet->setCellValueByColumnAndRow($position[$usr->getEvent()->getUid()], $row, 1);
                }
            }

            $sheet->setCellValueByColumnAndRow($column, $row, '=SUM(P' . $row . ':' . $sheet->getColumnDimensionByColumn($column - 1)->getColumnIndex() . $row . ')');

            $row++;
        }

        foreach (range(0, $column) as $col) {
            $sheet->getColumnDimensionByColumn($col)->setWidth(40);
            $sheet->getRowDimension(1)->setRowHeight(40);
            $sheet->getStyle('A1:'. $sheet->getColumnDimensionByColumn($column - 1)->getColumnIndex() . '1')->getAlignment()->setWrapText(true);
        }

        $this->response->setHeader('Content-Type', 'text/xlsx; charset=utf-8');
        $this->response->setHeader('Content-Disposition', 'attachment; filename="registrations.xls"');

        \PHPExcel_IOFactory::createWriter($excel, 'Excel5')->setPreCalculateFormulas(true)->save('php://output');
    }

    /**
     * @return array
     */
    protected function getRegistrations()
    {
        $querySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class);
        $querySettings->setRespectStoragePage(FALSE);
        $this->registrationRepository->setDefaultQuerySettings($querySettings);
        return $this->registrationRepository->findAll();
    }

    /**
     * @param array $elements
     * @param string $groupBy
     *
     * @return array
     */
    protected function groupElements($elements, $groupBy)
    {
        $groups = [];
        foreach ($elements as $key => $value) {
            if (is_array($value)) {
                $currentGroupIndex = isset($value[$groupBy]) ? $value[$groupBy] : null;
            } elseif (is_object($value)) {
                $currentGroupIndex = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getPropertyPath($value, $groupBy);
            }
            if (is_object($currentGroupIndex)) {
                if ($currentGroupIndex instanceof \TYPO3\CMS\Extbase\Persistence\Generic\LazyLoadingProxy) {
                    $currentGroupIndex = $currentGroupIndex->_loadRealInstance();
                }
                $currentGroupIndex = spl_object_hash($currentGroupIndex);
            }
            $groups[$currentGroupIndex][$key] = $value;
        }
        return $groups;
    }

    /**
     * Extract by key
     *
     * @param \Traversable $iterator
     * @param string $key
     * @return array
     */
    protected function extractByKey($iterator, $key)
    {
        $result = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getPropertyPath($iterator, $key);
        if ($result) {
            return $this->flattenArray($result);
        }

        $content = [];
        foreach ($iterator as $v) {
            $result = \TYPO3\CMS\Extbase\Reflection\ObjectAccess::getPropertyPath($v, $key);
            if (null !== $result) {
                $content[$result->getUid()] = $result;
            } elseif (true === is_array($v) || true === $v instanceof \Traversable) {
                $content[] = $this->recursivelyExtractKey($v, $key);
            }
        }

        return $this->flattenArray($content);
    }

    /**
     * Flatten the result structure, to iterate it cleanly in fluid
     *
     * @param array $content
     * @param array $flattened
     * @return array
     */
    protected function flattenArray(array $content, $flattened = null)
    {
        foreach ($content as $sub) {
            if (is_array($sub)) {
                $flattened = $this->flattenArray($sub, $flattened);
            } else {
                $flattened[] = $sub;
            }
        }

        return $flattened;
    }

}
