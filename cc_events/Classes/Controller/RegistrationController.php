<?php
namespace Netfed\CcEvents\Controller;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * RegistrationController
 */
class RegistrationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * eventRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\RegistrationRepository
     * @inject
     */
    protected $registrationRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * Initialize Action
     */
    public function initializeAction()
    {
        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration()
                ->allowAllProperties()
                ->skipUnknownProperties()
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                    TRUE
                )
                ->setTypeConverterOption(
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                    \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                    TRUE
                );
        }
    }

    /**
     * action index
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $event
     * @return void
     */
    public function indexAction(\Netfed\CcEvents\Domain\Model\Event $event)
    {
        if (!$event->isAvailable()) {
            $event = $event->getMainTermin();
        }

        $this->view->assign('event', $event);
        $this->view->assign('registration', $GLOBALS['TSFE']->fe_user->user);

        if ($this->objectManager->get(\Netfed\CcEvents\Domain\Repository\RegistrationRepository::class)->alreadyRegistered($event)) {
            return $this->view->render('confirmation');
        }
    }

    /**
     * action process
     *
     * @param \Netfed\CcEvents\Domain\Model\Registration $registration
     * @param \Netfed\CcEvents\Domain\Model\Event $event
     * @return void
     */
    public function processAction(\Netfed\CcEvents\Domain\Model\Registration $registration, \Netfed\CcEvents\Domain\Model\Event $event)
    {
        if (!$event->isAvailable()) {
            $this->forward('index', null, null, ['event' => $event->getMainTermin()]);
        }

        $registration->setEvent($event);
//        $registration->setHidden(true);
        $this->registrationRepository->add($registration);
        $this->persistenceManager->persistAll(); // $registration->getUid()

        $this->view->assign('registration', $registration);
        $this->view->assign('event', $event);

        $this->sendTemplatedMail(
            $this->settings['receiver'],
            $this->settings['sender'],
            $this->settings['subject'] . ' ' .$event->getTitle(),
            ['registration' => $registration, 'event' => $registration->getEvent()],
            'Mail/Admin'
        );

    }

    /**
     * Create HTML from variables and template path, send email
     *
     * @param string $receiver Email receiver
     * @param string $sender Email sender
     * @param string $subject Email subject
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     * @param string|array $attachment files to be attached
     * @param string $css file path of css
     */
    protected function sendTemplatedMail($receiver, $sender, $subject, $variables, $template, $attachment = NULL, $css = NULL) {
        $messageBody = $this->getStandaloneView($variables, $template, $css);
        $this->sendEmail($receiver, $sender, $subject, $messageBody, $attachment);
    }

    /**
     * render fluid standalone view
     *
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     * @param string $css file path of css
     *
     * @return string
     */
    protected function getStandaloneView($variables, $template, $css = NULL) {
        /** @var $standaloneView \TYPO3\CMS\Fluid\View\StandaloneView **/
        $standaloneView = $this->objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);
        $standaloneView->setControllerContext($this->controllerContext);
        $standaloneView->getRequest()->setControllerExtensionName($this->extensionName);
        $standaloneView->setFormat('html');
        $standaloneView->setLayoutRootPaths($this->settings['view']['layoutRootPaths']);
        $standaloneView->setTemplateRootPaths($this->settings['view']['templateRootPaths']);
        $standaloneView->setPartialRootPaths($this->settings['view']['partialRootPaths']);
        $standaloneView->setTemplate($template);
        $standaloneView->assignMultiple($variables);

        return $this->cssToInline($standaloneView->render(), $css);
    }

    /**
     * Css to Inline
     *
     * @param string $html
     * @param string $css
     *
     * @return string
     **/
    protected function cssToInline($html, $css = '') {
        if ($css = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($css ?: $this->settings['css'])) {
            $html = $this->objectManager->get(\Netfed\CcEvents\Service\CssToInline::class, $html, file_get_contents($css))->emogrify();
        }
        return $html;
    }

    /**
     * Send mail
     *
     * @param string $receiver
     * @param string $sender
     * @param string $subject
     * @param string $messageBody
     * @param mixed $attachment
     *
     * @return bool
     */
    protected function sendEmail($receiver, $sender, $subject, $messageBody, $attachment = NULL) {
        /** @var $message \TYPO3\CMS\Core\Mail\MailMessage **/
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $message->setTo(\TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $receiver))->setFrom(\TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', $sender))->setSubject($subject);
        $message->setBody($messageBody, 'text/html');

        if ($attachment) {
            $attachment = is_array($attachment) ? $attachment : [$attachment];
            foreach ($attachment as $attach) {
                $message->attach(\Swift_Attachment::fromPath($attach['url'] ?: $attach));
            }
        }

        $message->send();

        return $message->isSent();
    }

    /**
     * InitializeUpdate Action
     */
    public function initializeUpdateAction()
    {
        unset($GLOBALS['TCA']['tx_ccevents_domain_model_registration']['ctrl']['enablecolumns']['disabled']);
    }

    /**
     * action update
     *
     * @param \Netfed\CcEvents\Domain\Model\Registration $registration
     * @return void
     */
    public function updateAction(\Netfed\CcEvents\Domain\Model\Registration $registration)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $registration->setHidden(false);
        $this->registrationRepository->update($registration);

//        DebuggerUtility::var_dump($registration);die;
        $this->sendTemplatedMail(
            $registration->getEmail(),
            $this->settings['sender'],
            $this->settings['subject'],
            ['registration' => $registration, 'event' => $registration->getEvent()],
            'Mail/User'
        );

        $this->sendTemplatedMail(
            $this->settings['receiver'],
            $this->settings['sender'],
            $this->settings['subject'],
            ['registration' => $registration, 'event' => $registration->getEvent()],
            'Mail/Admin'
        );

        $this->redirect('index');
    }
}
