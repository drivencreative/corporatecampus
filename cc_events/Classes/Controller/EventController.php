<?php

namespace Netfed\CcEvents\Controller;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use Doctrine\Common\Util\Debug;
use Netfed\CcEvents\Domain\Model\FormatType;
use Netfed\CcEvents\Utility\ArrayTool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * EventController
 */
class EventController extends AbstractApiController
{
    /**
     * eventRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\EventRepository
     * @inject
     */
    protected $eventRepository = null;
    /**
     * pilarRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\PilarRepository
     * @inject
     */
    protected $pilarRepository = null;

    /**
     * partnerRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\PartnerRepository
     * @inject
     */
    protected $partnerRepository = null;

    /**
     * formatTypeRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\FormatTypeRepository
     * @inject
     */
    protected $formatTypeRepository = null;

    /**
     * datePeriodRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\DatePeriodRepository
     * @inject
     */
    protected $datePeriodRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $args = $this->request->getArguments();
        $events = $this->eventRepository->findAll();
        if ($args['filter']) {
            $events = $this->eventRepository->findByArgs($args['filter']);
        }
        $this->view->assign('args', $args);
        $this->view->assign('events', $events);

        if ($this->request->getFormat() == 'json') {
            $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
            $querySettings->setRespectStoragePage(FALSE);
            $this->pilarRepository->setDefaultQuerySettings($querySettings);
        }
        $pilars = $this->pilarRepository->findAll();
//        $formats = $this->formatTypeRepository->findAll();
//        $formats = [];
//        foreach ($GLOBALS['TSFE']->fe_user->groupData['uid'] as $userGroup) {
//            $formats = array_merge($formats, $this->formatTypeRepository->findByUserGroup($userGroup));
//        }
        $formats = $this->formatTypeRepository->findByUserGroup($GLOBALS['TSFE']->fe_user->groupData['uid']);
//        DebuggerUtility::var_dump(array_filter($formats));die;
        $this->view->assign('pilars', $pilars);
        $this->view->assign('formats', $formats);
        $this->view->assign('events', $events);
        $this->jsonView([
            'pilars' => [
                '_descendAll' => [
                    '_exclude' => ['pid'],
                    '_descend' => [
                        'formatType' => [
                            '_exclude' => ['pid','feGroups'],
                            '_descendAll' => [
                                '_exclude' => ['pid'],
                                '_descend' => [
                                    'events' => [
                                        '_exclude' => ['pid'],
                                        '_descendAll' => [
                                            '_exclude' => ['pid'],
                                            '_descend' => [
                                                'files' => [
                                                    '_exclude' => ['pid'],
                                                    '_descendAll' => [
                                                        '_exclude' => ['pid'],
                                                    ]
                                                ],
                                                'formatType' => [
                                                    '_descendAll' => [
                                                        '_exclude' => ['pid','feGroups'],
                                                        'pilar' => [
                                                            '_exclude' => ['pid'],
                                                            '_descend' => [
                                                                '_exclude' => ['pid'],
                                                            ]
                                                        ],
                                                    ]
                                                ],
                                                'partner' => [
                                                    '_exclude' => ['pid', 'uid'],
                                                    '_descendAll' => [
                                                        '_exclude' => ['pid'],
                                                        '_descend' => [
                                                            'image' => [
                                                                '_exclude' => ['pid'],
                                                                '_descendAll' => [
                                                                    '_exclude' => ['pid'],
                                                                ]
                                                            ],
                                                        ],
                                                    ]
                                                ],
                                            ]
                                        ]
                                    ],
                                ]
                            ]
                        ],
                    ]
                ]
            ]
        ]);
    }

    /**
     * action download
     *
     * @return void
     */
    public function downloadAction()
    {
        $args = $this->request->getArguments();
        $properties = ['title', 'text', 'city', 'hotel', 'componentmodules', 'dress_code'];
        $events = $this->eventRepository->findByArgs($args['filter']);
        $partners = $this->partnerRepository->findAll();
        $datePeriods = $this->datePeriodRepository->findAll();
//        $formatTypes = $this->formatTypeRepository->findAll();
        $formatTypes = $this->formatTypeRepository->filterByUids(GeneralUtility::trimExplode(',', $this->settings['formatTypesForDownloads'], true));
//        $formatTypes = $this->getFormatTypesWithData($events);
        $this->view->assign('args', $args);
        $this->view->assign('events', $events);
        $this->view->assign('partners', $partners);
        $this->view->assign('formats', $formatTypes);
        $this->view->assign('datePeriods', $datePeriods);
    }

    /**
     * action show
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $event
     * @return void
     */
    public function showAction(\Netfed\CcEvents\Domain\Model\Event $event)
    {
        $pilars = $this->pilarRepository->findAll();
        $this->view->assign('pilars', $pilars);
        $this->view->assign('event', $event);
    }

    /**
     * action file
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file
     * @return void
     */
    public function fileAction(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file)
    {
        $file->getOriginalResource()->getOriginalFile()->getStorage()->dumpFileContents($file->getOriginalResource()->getOriginalFile());
    }

    /**
     * action create
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $newEvent
     * @return void
     */
    public function createAction(\Netfed\CcEvents\Domain\Model\Event $newEvent)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->eventRepository->add($newEvent);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $event
     * @ignorevalidation $event
     * @return void
     */
    public function editAction(\Netfed\CcEvents\Domain\Model\Event $event)
    {
        $this->view->assign('event', $event);
    }

    /**
     * action update
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $event
     * @return void
     */
    public function updateAction(\Netfed\CcEvents\Domain\Model\Event $event)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->eventRepository->update($event);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $event
     * @return void
     */
    public function deleteAction(\Netfed\CcEvents\Domain\Model\Event $event)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->eventRepository->remove($event);
        $this->redirect('list');
    }

    /**
     * action listSlider
     *
     * @return void
     */
    public function listSliderAction()
    {
        $events = $this->eventRepository->findByNearestInTheFuture();
        $this->view->assign('events', $events);
    }

    /**
     * action listSlider
     *
     * @return void
     */
    public function formatsReviewAction()
    {
        $pilars = $this->pilarRepository->findAll();
        $this->view->assign('pilars', $pilars);
    }

    /**
     * action calendar
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $event
     * @return void
     */
    public function calendarAction(\Netfed\CcEvents\Domain\Model\Event $event)
    {
        /** @var $vCalendar \Eluceo\iCal\Component\Calendar **/
        $vCalendar = $this->objectManager->get(\Eluceo\iCal\Component\Calendar::class, $this->request->getBaseUri());
        /** @var $vEvent \Eluceo\iCal\Component\Event **/
        $vEvent = $this->objectManager->get(\Eluceo\iCal\Component\Event::class);

        /** @var Description Data */
        $dressCode = $event->getDressCode()? '<h3>'.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_ccevents_domain_model_event.dress_code','cc_events').'</h3> <p>'.$event->getDressCode().'</p>':'';
        $procedure = $event->getProcedure()? '<h3>'.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_ccevents_domain_model_event.procedure','cc_events').'</h3> '.$event->getProcedure():'';
        $hotel = $event->getHotel()? '<h3>'.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_ccevents_domain_model_event.hotel','cc_events').'</h3> '.$event->getHotel():'';
        $venue = $event->getVenue()? '<h3>'.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_ccevents_domain_model_event.venue','cc_events').'</h3> '.$event->getVenue():'';
        $venueAndHotel = $event->getVenueAndHotel()? '<h3>'.\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_ccevents_domain_model_event.venue_and_hotel','cc_events').'</h3> '.$event->getVenueAndHotel():'';

        /** @var HTML format $descriptionHTML */
        $descriptionHTML = $dressCode . $procedure . $hotel . $venue . $venueAndHotel;
        /** @var Plain text format $description */
        $description = strip_tags(strip_tags($dressCode) .PHP_EOL. strip_tags($procedure) .PHP_EOL. strip_tags($hotel) .PHP_EOL. strip_tags($venue) .PHP_EOL. strip_tags($venueAndHotel));

        /** set Start & End date */
        $vEvent->setUseTimezone(true);
        $vEvent->setDtStart($event->getStartDate())->setDtEnd($event->getEndDate())->setIsPrivate(true);

        $vEvent->setSummary('Corporate Campus - ' . $event->getTitle())->setDescriptionHTML($descriptionHTML)->setDescription(html_entity_decode($description, ENT_QUOTES, 'UTF-8'));
        $location = html_entity_decode(strip_tags(str_replace(['<br />','<br/>', '< /p>','</p>'],'//',$event->getVenue())), ENT_QUOTES, 'UTF-8');

        /** set Location */
        $vEvent->setLocation($location ?: '');

        /** add Event */
        $vCalendar->addComponent($vEvent);

        /** set Headers */
        $this->response->setHeader('Content-Disposition', 'attachment; filename="calendar.ics"');
        $this->response->setHeader('Content-Type', 'text/calendar; charset=UTF-8');
        $this->response->setHeader('Pragma', 'no-cache');
        $this->response->setHeader('Expires', '0');

        $this->response->setContent($vCalendar->render());

        $this->response->send();exit();
    }

    /**
     * Filter only formatTypes which has events
     *
     * @param $events
     * @return array
     */
    public function getFormatTypesWithData($events)
    {
        $formatTypes = [];
        if (is_array(ArrayTool::flattenArray(ArrayTool::extractByKey($events, 'formatType')))) {
            $formatTypesFromEvents = array_unique(ArrayTool::flattenArray(ArrayTool::extractByKey($events, 'formatType')));
            if ($formatTypesFromEvents) {
                /** @var FormatType $formatType */
                foreach ($formatTypesFromEvents as $formatType) {
                    if (in_array($formatType->getUid(), GeneralUtility::trimExplode(',', $this->settings['formatTypesForDownloads'], true))) {
                        $formatTypes[] = $formatType;
                    }
                }
            }
        }
        return $formatTypes;
    }

}
