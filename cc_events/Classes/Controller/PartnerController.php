<?php
namespace Netfed\CcEvents\Controller;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * PartnerController
 */
class PartnerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * partnerRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\PartnerRepository
     * @inject
     */
    protected $partnerRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $partners = $this->partnerRepository->findAll();
        $this->view->assign('partners', $partners);
    }

    /**
     * action show
     *
     * @param \Netfed\CcEvents\Domain\Model\Partner $partner
     * @return void
     */
    public function showAction(\Netfed\CcEvents\Domain\Model\Partner $partner)
    {
        $this->view->assign('partner', $partner);
    }
}
