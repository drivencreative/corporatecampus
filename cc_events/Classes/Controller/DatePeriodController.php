<?php
namespace Netfed\CcEvents\Controller;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * DatePeriodController
 */
class DatePeriodController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * datePeriodRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\DatePeriodRepository
     * @inject
     */
    protected $datePeriodRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $datePeriods = $this->datePeriodRepository->findAll();
        $this->view->assign('datePeriods', $datePeriods);
    }
}
