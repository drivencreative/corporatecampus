<?php
namespace Netfed\CcEvents\Controller;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * PilarController
 */
class PilarController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $pilars = $this->pilarRepository->findAll();
        $this->view->assign('pilars', $pilars);
    }

    /**
     * action show
     *
     * @param \Netfed\CcEvents\Domain\Model\Pilar $pilar
     * @return void
     */
    public function showAction(\Netfed\CcEvents\Domain\Model\Pilar $pilar)
    {
        $this->view->assign('pilar', $pilar);
    }
}
