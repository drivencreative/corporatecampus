<?php
namespace Netfed\CcEvents\Controller;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * FormatTypeController
 */
class FormatTypeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * formatTypeRepository
     *
     * @var \Netfed\CcEvents\Domain\Repository\FormatTypeRepository
     * @inject
     */
    protected $formatTypeRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $forms = $this->formatTypeRepository->findAll();
        $this->view->assign('forms', $forms);
    }

    /**
     * action show
     *
     * @param \Netfed\CcEvents\Domain\Model\FormatType $formatType
     * @return void
     */
    public function showAction(\Netfed\CcEvents\Domain\Model\FormatType $formatType)
    {
        $this->view->assign('form', $formatType);
    }
}
