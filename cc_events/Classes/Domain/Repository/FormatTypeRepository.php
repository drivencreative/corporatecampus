<?php
namespace Netfed\CcEvents\Domain\Repository;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Forms
 */
class FormatTypeRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    public function findByUsergroup($usergroups)
    {
        if (count($usergroups)) {
            $query = $this->createQuery();
            $constraints = [];
            foreach ($usergroups AS $usergroup) {
                $constraints[] = $query->contains('feGroups', $usergroup);
            }
            return $query->matching(
                $query->logicalOr($constraints)
            )->execute()->toArray();
        }
        return [];
    }

    /**
     * @param $uids
     * @return array|EventRepository|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function filterByUids($uids)
    {
        $query = $this->createQuery();
        $query->matching($query->in('uid', $uids));
        return $query->execute();
    }
}
