<?php
namespace Netfed\CcEvents\Domain\Repository;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Registration
 */
class RegistrationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * initialize repository
     */
    public function initializeObject()
    {
        /** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
        $querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
        $querySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * Check if current user is already registered for event
     */
    public function alreadyRegistered($event) {
        $query = $this->createQuery();
        return $query->matching($query->logicalAnd(
            $query->equals('event', $event),
            $query->equals('user', $GLOBALS['TSFE']->fe_user->user['uid'])
        ))->execute()->count();
    }
}
