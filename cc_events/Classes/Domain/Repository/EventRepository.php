<?php

namespace Netfed\CcEvents\Domain\Repository;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use Netfed\CcEvents\Domain\Model\DatePeriod;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Events
 */
class EventRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * initialize Object
     **/
    public function initializeObject()
    {
        $defaultQuerySettings = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface::class);
    }

    /**
     * @param array $args
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByArgs($args = [])
    {
        $query = $this->createQuery();
        $constraintsAnd = $this->filter($args ?: [], $query);
        $constraintsAnd[] = $query->logicalNot($query->equals('downloads', 0));
        if ($constraintsAnd) {
            $query->matching(
                $query->logicalAnd(
                    $constraintsAnd
                )
            );
        }

        return $query->execute();
    }

    /**
     * Replace the like query wildcards for a clean database selection
     *
     * @param string $propertyWithoutWildcard
     *
     * @return string
     */
    protected function escapeLikeQuery($propertyWithoutWildcard)
    {
        return str_replace(array (
            '%',
            '_'
        ), array (
            '\\%',
            '\\_'
        ), $propertyWithoutWildcard);
    }

    /**
     * @param array $args
     * @param @param \TYPO3\CMS\Extbase\Persistence\QueryInterface $query
     * @return array
     */
    public function filter($args = [], \TYPO3\CMS\Extbase\Persistence\QueryInterface $query)
    {
        $constraints = $constraintsOr = [];
        foreach ($args as $field => $value) {
            if (!empty($value)) {
                switch ($field) {
                    case 'format_type':
                        $formatType = [];
                        foreach ((is_array($value) ? $value : [$value]) as $v) {
                            $formatType[] = $query->contains('formatType', $v);
                        }
                        if ($formatType) $constraints[] = $query->logicalOr($formatType);
                        break;
                    case 'date_period':
                        $periods = GeneralUtility::makeInstance(\Netfed\CcEvents\Domain\Repository\DatePeriodRepository::class)->findByUids($value);
                        /**
                         * @var  $val DatePeriod
                         */
                        foreach ($periods as $key => $val) {
                            $constraintsOr[] = $query->logicalOr(
                                $query->logicalAnd(
                                    $query->greaterThanOrEqual("start_date", (new \DateTime())->setDate($val->getYear(), 1, 1)),
                                    $query->lessThanOrEqual("end_date", (new \DateTime())->setDate($val->getYear(), 12, 31))
                                )
                            );
                        }
                        break;
                    case 'limit':
                        $query->setLimit((int)$value);
                        break;
                    case 'storagePage':
                        $query->setQuerySettings($query->getQuerySettings()->setRespectStoragePage($value));
                        break;
                    case 'sword':
                        foreach (['title', 'text', 'city', 'hotel', 'componentmodules', 'dress_code', 'downloads.title', 'downloads.description', 'downloads.author'] as $property) {
                            $constraintsOr[] = $query->like($property, '%' . $this->escapeLikeQuery($value) . '%', FALSE);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        if ($constraintsOr) {
            $constraints[] = $query->logicalOr($constraintsOr);
        }
        return $constraints;
    }

    /**
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByNearestInTheFuture()
    {
        $query = $this->createQuery();

        $query->setOrderings(['startDate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING]);

        $query->matching(
            $query->logicalAnd(
                $query->greaterThanOrEqual("start_date", (new \DateTime()))
            )
        );

        return $query->execute();
    }

    /**
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findActive()
    {
        $query = $this->createQuery();
        $date = new \DateTime('midnight');
        $query->matching(
            $query->logicalOr(
                $query->greaterThanOrEqual('end_date', $date->format('Y-m-d H:i:s')),
                $query->greaterThanOrEqual('start_date', $date->format('Y-m-d H:i:s'))
            )
        );


        return $query->execute();
    }
}
