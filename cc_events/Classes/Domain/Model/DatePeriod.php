<?php
namespace Netfed\CcEvents\Domain\Model;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * DatePeriod
 */
class DatePeriod extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * fromDate
     *
     * @var \DateTime
     */
    protected $fromDate = null;

    /**
     * toDate
     *
     * @var \DateTime
     */
    protected $toDate = '';

    /**
     * year
     *
     * @var int
     */
    protected $year = 0;

    /**
     * Returns the fromDate
     *
     * @return \DateTime $fromDate
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Sets the fromDate
     *
     * @param \DateTime $fromDate
     * @return void
     */
    public function setFromDate(\DateTime $fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * Returns the toDate
     *
     * @return \DateTime $toDate
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Sets the toDate
     *
     * @param \DateTime $toDate
     * @return void
     */
    public function setToDate(\DateTime $toDate)
    {
        $this->toDate = $toDate;
    }

    /**
     * Returns the year
     *
     * @return int $year
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Sets the year
     *
     * @param int $year
     * @return void
     */
    public function setYear($year)
    {
        $this->year = $year;
    }
}
