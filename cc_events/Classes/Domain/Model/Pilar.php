<?php
namespace Netfed\CcEvents\Domain\Model;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Pilar
 */
class Pilar extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * subtext
     *
     * @var string
     */
    protected $subtext = '';

    /**
     * formatType
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FormatType>
     */
    protected $formatType = null;

    /**
     * link
     *
     * @var string
     */
    protected $link = '';

    /**
     * label
     *
     * @var string
     */
    protected $label = '';

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the subtext
     *
     * @return string $subtext
     */
    public function getSubtext()
    {
        return $this->subtext;
    }

    /**
     * Sets the subtext
     *
     * @param string $subtext
     * @return void
     */
    public function setSubtext($subtext)
    {
        $this->subtext = $subtext;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->formatType = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a FormatType
     *
     * @param \Netfed\CcEvents\Domain\Model\FormatType $formatType
     * @return void
     */
    public function addFormatType(\Netfed\CcEvents\Domain\Model\FormatType $formatType)
    {
        $this->formatType->attach($formatType);
    }

    /**
     * Removes a FormatType
     *
     * @param \Netfed\CcEvents\Domain\Model\FormatType $formatTypeToRemove The FormatType to be removed
     * @return void
     */
    public function removeFormatType(\Netfed\CcEvents\Domain\Model\FormatType $formatTypeToRemove)
    {
        $this->formatType->detach($formatTypeToRemove);
    }

    /**
     * Returns the formatType
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FormatType> $formatType
     */
    public function getFormatType()
    {
        return $this->formatType;
    }

    /**
     * Sets the formatType
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FormatType> $formatType
     * @return void
     */
    public function setFormatType(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $formatType)
    {
        $this->formatType = $formatType;
    }

    /**
     * Returns the link
     *
     * @return string $link
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Sets the link
     *
     * @param string $link
     * @return void
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * Returns the label
     *
     * @return string $label
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Sets the label
     *
     * @param string $label
     * @return void
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }
}
