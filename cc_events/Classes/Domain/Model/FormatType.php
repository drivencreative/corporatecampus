<?php
namespace Netfed\CcEvents\Domain\Model;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Form
 */
class FormatType extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * events
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Event>
     */
    protected $events = null;

    /**
     * pilar
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Pilar>
     */
    protected $pilar = null;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * fe_groups
     *
     * @var string
     */
    protected $feGroups;


    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->events = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Event
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $event
     * @return void
     */
    public function addEvent(\Netfed\CcEvents\Domain\Model\Event $event)
    {
        $this->events->attach($event);
    }

    /**
     * Removes a Event
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $eventToRemove The Event to be removed
     * @return void
     */
    public function removeEvent(\Netfed\CcEvents\Domain\Model\Event $eventToRemove)
    {
        $this->events->detach($eventToRemove);
    }

    /**
     * Returns the events
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Event> $events
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Sets the events
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Event> $events
     * @return void
     */
    public function setEvents(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $events)
    {
        $this->events = $events;
    }

    /**
     * Returns the pilar
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Pilar> $pilar
     */
    public function getPilar()
    {
        return $this->pilar;
    }

    /**
     * Sets the pilar
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Pilar> $pilar
     * @return void
     */
    public function setPilar(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $pilar)
    {
        $this->pilar = $pilar;
    }

    /**
     * @param string $feGroups
     */
    public function setFeGroups($feGroups)
    {
        $this->feGroups = $feGroups;
    }

    /**
     * @return string
     */
    public function getFeGroups()
    {
        return $this->feGroups;
    }

}
