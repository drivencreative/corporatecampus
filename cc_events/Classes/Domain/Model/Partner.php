<?php
namespace Netfed\CcEvents\Domain\Model;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use \TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;

/**
 * Partner
 */
class Partner extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Image processing
     *
     * @return string
     */
    public function getImageUrl() {
        $settings = [];
        if ($this->image) {
            $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
            $configurationManager = $objectManager->get(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::class);
            $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);

            /** @var $imageService \TYPO3\CMS\Extbase\Service\ImageService **/
            $imageService = $objectManager->get(\TYPO3\CMS\Extbase\Service\ImageService::class);
            try {
                $image = $imageService->getImage($this->image->getUid(), $this->image, 0);

                $cropString = $image->hasProperty('crop') ? $image->getProperty('crop') : '';
                $cropVariantCollection = CropVariantCollection::create((string)$cropString);
                $cropVariant = $settings['cropVariant'] ?: 'default';
                $cropArea = $cropVariantCollection->getCropArea($cropVariant);
                $processingInstructions = [
                    'maxWidth' => $settings['maxWidth'],
                    'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
                ];
                $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
                $imageUri = $imageService->getImageUri($processedImage, 1);

                return $imageUri;
            } catch (\TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException $e) {
                // thrown if file does not exist
            } catch (\UnexpectedValueException $e) {
                // thrown if a file has been replaced with a folder
            } catch (\RuntimeException $e) {
                // RuntimeException thrown if a file is outside of a storage
            } catch (\InvalidArgumentException $e) {
                // thrown if file storage does not exist
            }
        }
    }
}
