<?php

namespace Netfed\CcEvents\Domain\Model;


class Content extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * uid
     *
     * @var string
     */
    protected $uid = '';

    /**
     * pid
     *
     * @var string
     */
    protected $pid = '';

    /**
     * header
     *
     * @var string
     */
    protected $header = '';

    /**
     * sorting
     *
     * @var string
     */
    protected $sorting = '';

    /**
     * contentType
     *
     * @var string
     */
    protected $contentType = '';

    /**
     * Gets the uid
     *
     * @return string $uid
     */
    public function getUid() {
        return $this->uid;
    }
    /**
     * Gets the pid
     *
     * @return string $pid
     */
    public function getPid() {
        return $this->pid;
    }

    /**
     * Returns the header
     *
     * @return string $header
     */
    public function getHeader() {
        return $this->header;
    }

    /**
     * Sets the header
     *
     * @param string $header
     * @return void
     */
    public function setHeader($header) {
        $this->header = $header;
    }

    /**
     * Returns the sorting
     *
     * @return string $sorting
     */
    public function getSorting() {
        return $this->sorting;
    }

    /**
     * Sets the sorting
     *
     * @param string $sorting
     * @return void
     */
    public function setSorting($sorting) {
        $this->sorting = $sorting;
    }

    /**
     * Returns the contentType
     *
     * @return string $contentType
     */
    public function getContentType() {
        return $this->contentType;
    }

    /**
     * Sets the contentType
     *
     * @param string $contentType
     * @return void
     */
    public function setContentType($contentType) {
        $this->contentType = $contentType;
    }

}
