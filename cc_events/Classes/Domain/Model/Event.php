<?php

namespace Netfed\CcEvents\Domain\Model;

/***
 *
 * This file is part of the "Corporate Campus" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Event
 */
class Event extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * subtitle
     *
     * @var string
     */
    protected $subtitle = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * startDate
     *
     * @var \DateTime
     */
    protected $startDate = null;

    /**
     * endDate
     *
     * @var \DateTime
     */
    protected $endDate = null;

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * price
     *
     * @var string
     */
    protected $price = '';

    /**
     * hotel
     *
     * @var string
     */
    protected $hotel = '';

    /**
     * dressCode
     *
     * @var string
     */
    protected $dressCode = '';

    /**
     * downloads
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FileReference>
     * @cascade remove
     */
    protected $downloads = null;

    /**
     * partner
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Partner>
     */
    protected $partner = null;

    /**
     * datePeriod
     *
     * @var \Netfed\CcEvents\Domain\Model\DatePeriod
     */
    protected $datePeriod = null;

    /**
     * formatType
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FormatType>
     */
    protected $formatType = null;

    /**
     * terminSeries
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Event>
     * @lazy
     */
    protected $terminSeries = null;

    /**
     * mainTermin
     *
     * @var \Netfed\CcEvents\Domain\Model\Event
     */
    protected $mainTermin = null;

    /**
     * series
     *
     * @var string
     */
    protected $series = '';

    /**
     * participantsGroup
     *
     * @var string
     */
    protected $participantsGroup = '';

    /**
     * numberOfParticipants
     *
     * @var string
     */
    protected $numberOfParticipants = '';

    /**
     * requirement
     *
     * @var string
     */
    protected $requirement = '';

    /**
     * procedure
     *
     * @var string
     */
    protected $procedure = '';

    /**
     * venue
     *
     * @var string
     */
    protected $venue = '';

    /**
     * venueAndHotel
     *
     * @var string
     */
    protected $venueAndHotel = '';

    /**
     * dates
     *
     * @var string
     */
    protected $dates = '';

    /**
     * registrationInfo
     *
     * @var string
     */
    protected $registrationInfo = '';

    /**
     * @var int
     */
    protected $active = 0;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->downloads = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->formatType = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->terminSeries = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->title = $subtitle;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getTeaser()
    {
        return html_entity_decode(substr(strip_tags($this->text),0,180).'...', ENT_HTML401, 'UTF-8');
    }

    /**
     * Returns the startDate
     *
     * @return \DateTime $startDate
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Sets the startDate
     *
     * @param \DateTime $startDate
     * @return void
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * Returns the endDate
     *
     * @return \DateTime $endDate
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Returns the formatedDate
     *
     * @return string $startDate
     */
    public function getFormatedDate()
    {
        if ($this->startDate && $this->endDate) {
            if ($this->startDate->format('Ymd') == $this->endDate->format('Ymd')) {
                return strftime('%e. %B %Y', $this->startDate->getTimestamp());
            }
            return strftime('%e. %B', $this->startDate->getTimestamp()) . ' - ' . strftime('%e. %B %Y', $this->endDate->getTimestamp());
        }
        return strftime('%e. %B %Y', $this->startDate->getTimestamp());
    }


    /**
     * Returns the formatedDate
     *
     * @return string $startDate
     */
    public function getFormatedDateTime()
    {
        if ($this->startDate && $this->endDate) {
            if ($this->startDate->format('Ymd') == $this->endDate->format('Ymd')) {
                return strftime('%e. %B %Y %H:%M', $this->startDate->getTimestamp()) . ' - ' . strftime('%H:%M Uhr', $this->endDate->getTimestamp());
            }
            return strftime('%e. %B', $this->startDate->getTimestamp()) . ' - ' . strftime('%e. %B %Y', $this->endDate->getTimestamp());
        }
        return strftime('%e. %B %Y', $this->startDate->getTimestamp());
    }

    /**
     * Returns the startDate
     *
     * @return \DateTime $startDate
     */
    public function getStartTimeDate()
    {
        return $this->startDate->getTimestamp();
    }

    /**
     * Returns the endDate
     *
     * @return \DateTime $endDate
     */
    public function getEndTimeDate()
    {
        return $this->endDate ? $this->endDate->getTimestamp() : '';
    }

    /**
     * Sets the endDate
     *
     * @param \DateTime $endDate
     * @return void
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the price
     *
     * @return string $price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Sets the price
     *
     * @param string $price
     * @return void
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Returns the hotel
     *
     * @return string $hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Sets the hotel
     *
     * @param string $hotel
     * @return void
     */
    public function setHotel($hotel)
    {
        $this->hotel = $hotel;
    }

    /**
     * Returns the dressCode
     *
     * @return string $dressCode
     */
    public function getDressCode()
    {
        return $this->dressCode;
    }

    /**
     * Sets the dressCode
     *
     * @param string $dressCode
     * @return void
     */
    public function setDressCode($dressCode)
    {
        $this->dressCode = $dressCode;
    }

    /**
     * Adds a FileReference
     *
     * @param \Netfed\CcEvents\Domain\Model\FileReference $download
     * @return void
     */
    public function addDownload(\Netfed\CcEvents\Domain\Model\FileReference $download)
    {
        $this->downloads->attach($download);
    }

    /**
     * Removes a FileReference
     *
     * @param \Netfed\CcEvents\Domain\Model\FileReference $downloadToRemove The FileReference to be removed
     * @return void
     */
    public function removeDownload(\Netfed\CcEvents\Domain\Model\FileReference $downloadToRemove)
    {
        $this->downloads->detach($downloadToRemove);
    }

    /**
     * Returns the downloads
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FileReference> $downloads
     */
    public function getDownloads()
    {
        $downloads = clone $this->downloads;
        foreach ($downloads as $download) {
            if (!$download->isAllowed()) {
                $downloads->detach($download);
            }
        }
        return $downloads;
    }

    /**
     * Returns the files
     *
     * @return array $files
     */
    public function getFiles()
    {
        $files = [];
        /** @var $uriBuilder \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder * */
        $uriBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class)->get(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder::class);

        foreach ($this->getDownloads() as $download) {
            $files[] = [
                'url' => $uriBuilder->setTargetPageType(19852604)->setCreateAbsoluteUri(true)->setArguments(['tx_ccevents_events' => ['action' => 'file', 'file' => $download->getUid()]])->build(),
                'size' => $download->getOriginalResource()->getSize(),
                'ext' => $download->getOriginalResource()->getExtension(),
                'title' => $download->getOriginalResource()->getTitle(),
                'author' => $download->getAuthor(),
            ];
        }
        return $files;
    }

    /**
     * Sets the downloads
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FileReference> $downloads
     * @return void
     */
    public function setDownloads(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $downloads)
    {
        $this->downloads = $downloads;
    }

    /**
     * Returns the partner
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Partner> $partner
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * Sets the partner
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Partner> $partner
     * @return void
     */
    public function setPartner(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $partner)
    {
        $this->partner = $partner;
    }

    /**
     * Returns the datePeriod
     *
     * @return \Netfed\CcEvents\Domain\Model\DatePeriod $datePeriod
     */
    public function getDatePeriod()
    {
        return $this->datePeriod;
    }

    /**
     * Sets the datePeriod
     *
     * @param \Netfed\CcEvents\Domain\Model\DatePeriod $datePeriod
     * @return void
     */
    public function setDatePeriod(\Netfed\CcEvents\Domain\Model\DatePeriod $datePeriod)
    {
        $this->datePeriod = $datePeriod;
    }

    /**
     * Adds a formatType
     *
     * @param \Netfed\CcEvents\Domain\Model\FormatType $formatType
     * @return void
     */
    public function addFormatType(\Netfed\CcEvents\Domain\Model\FormatType $formatType)
    {
        $this->formatType->attach($formatType);
    }

    /**
     * Removes a formatType
     *
     * @param \Netfed\CcEvents\Domain\Model\FormatType $formatTypeToRemove The FormatType to be removed
     * @return void
     */
    public function removeFormatType(\Netfed\CcEvents\Domain\Model\FormatType $formatTypeToRemove)
    {
        $this->formatType->detach($formatTypeToRemove);
    }

    /**
     * Returns the formatType
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FormatType> $formatType
     */
    public function getFormatType()
    {
        return $this->formatType;
    }

    /**
     * Sets the formatType
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\FormatType> $formatType
     * @return void
     */
    public function setFormatType(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $formatType)
    {
        $this->formatType = $formatType;
    }

    /**
     * Adds a terminSeries
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $terminSeries
     * @return void
     */
    public function addTerminSeries(\Netfed\CcEvents\Domain\Model\Event $terminSeries)
    {
        $this->terminSeries->attach($terminSeries);
    }

    /**
     * Removes a terminSeries
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $terminSeriesToRemove The Event to be removed
     * @return void
     */
    public function removeTerminSeries(\Netfed\CcEvents\Domain\Model\Event $terminSeriesToRemove)
    {
        $this->terminSeries->detach($terminSeriesToRemove);
    }

    /**
     * Returns the terminSeries
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Event> $terminSeries
     */
    public function getTerminSeries()
    {
        return $this->terminSeries;
    }

    /**
     * Sets the terminSeries
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Netfed\CcEvents\Domain\Model\Event> $terminSeries
     * @return void
     */
    public function setTerminSeries(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $terminSeries)
    {
        $this->terminSeries = $terminSeries;
    }

    /**
     * Returns the mainTermin
     *
     * @return \Netfed\CcEvents\Domain\Model\Event $mainTermin
     */
    public function getMainTermin()
    {
        return $this->mainTermin;
    }

    /**
     * Sets the mainTermin
     *
     * @param \Netfed\CcEvents\Domain\Model\Event $mainTermin
     * @return void
     */
    public function setMainTermin(\Netfed\CcEvents\Domain\Model\Event $mainTermin)
    {
        $this->mainTermin = $mainTermin;
    }

    /**
     * Returns the available status
     *
     * @return bool $available
     */
    public function isAvailable()
    {
        return !(bool)$this->mainTermin;
    }

    /**
     * Returns the series
     *
     * @return string
     */
    public function getSeries()
    {
        return $this->series;
    }

    /**
     * Sets the series
     *
     * @param string $series
     * @return void
     */
    public function setSeries($series)
    {
        $this->series = $series;
    }

    /**
     * Returns the url
     *
     * @return string
     */
    public function getUrl()
    {
        /** @var $uriBuilder \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder * */
        $uriBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        $pageUid = $GLOBALS['TSFE']->id;
        return $uriBuilder  ->get(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder::class)
                            ->setTargetPageUid($pageUid)
                            ->setCreateAbsoluteUri(true)
                            ->setArguments(
                                [
                                    'tx_ccevents_events' =>
                                        [
                                            'event' => $this->getUid(),
                                            'action' => 'show',
                                            'controller' => 'Event'
                                        ]
                                ]
                            )
                            ->build();
    }

    /**
     * Returns the registerEventUrl
     *
     * @return string
     */
    public function getRegisterEventUrl()
    {
        /** @var $uriBuilder \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder * */
        $uriBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class)->get(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder::class);
        $pageUid = $GLOBALS['TSFE']->id;
        return $uriBuilder->reset()
                            ->setTargetPageUid($pageUid)
                            ->setCreateAbsoluteUri(true)
                            ->setTargetPageType(226688)
                            ->setArguments([
                                'tx_ccevents_form' => [
                                    'event' => $this->getUid(),
                                    'action' => 'index',
                                    'controller' => 'Registration'
                                ]
                            ])->build();
    }

    /**
     * Returns the terminImportUrl
     *
     * @return string
     */
    public function getTerminImportUrl()
    {
        /** @var $uriBuilder \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder * */
        $uriBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class)->get(\TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder::class);
        $pageUid = $GLOBALS['TSFE']->id;
        return $uriBuilder->reset()
                            ->setTargetPageUid($pageUid)
                            ->setCreateAbsoluteUri(true)
                            ->setTargetPageType(19852604)
                            ->uriFor('calendar', ['event' => $this->getUid(), 'format' => 'ics'], 'Event', 'ccevents', 'events');
    }

    /**
     * @return string
     */
    public function getParticipantsGroup ()
    {
        return $this->participantsGroup;
    }

    /**
     * @param string $participantsGroup
     */
    public function setParticipantsGroup ($participantsGroup)
    {
        $this->participantsGroup = $participantsGroup;
    }

    /**
     * @return string
     */
    public function getNumberOfParticipants ()
    {
        return $this->numberOfParticipants;
    }

    /**
     * @param string $numberOfParticipants
     */
    public function setNumberOfParticipants ($numberOfParticipants)
    {
        $this->numberOfParticipants = $numberOfParticipants;
    }

    /**
     * @return string
     */
    public function getRequirement ()
    {
        return $this->requirement;
    }

    /**
     * @param string $requirement
     */
    public function setRequirement ($requirement)
    {
        $this->requirement = $requirement;
    }

    /**
     * @return string
     */
    public function getProcedure ()
    {
        return $this->procedure;
    }

    /**
     * @param string $procedure
     */
    public function setProcedure ($procedure)
    {
        $this->procedure = $procedure;
    }

    /**
     * @return string
     */
    public function getVenue ()
    {
        return $this->venue;
    }

    /**
     * @param string $venue
     */
    public function setVenue ($venue)
    {
        $this->venue = $venue;
    }

    /**
     * @return string
     */
    public function getVenueAndHotel ()
    {
        return $this->venueAndHotel;
    }

    /**
     * @param string $venueAndHotel
     */
    public function setVenueAndHotel ($venueAndHotel)
    {
        $this->venueAndHotel = $venueAndHotel;
    }

    /**
     * @return string
     */
    public function getDates ()
    {
        return $this->dates;
    }

    /**
     * @param string $dates
     */
    public function setDates ($dates)
    {
        $this->dates = $dates;
    }

    /**
     * @return string
     */
    public function getRegistrationInfo ()
    {
        return $this->registrationInfo;
    }

    /**
     * @param string $registrationInfo
     */
    public function setRegistrationInfo ($registrationInfo)
    {
        $this->registrationInfo = $registrationInfo;
    }

    /**
     * @return int
     */
    public function getActive() {
        if ($this->getEndTimeDate()){
            $this->active = (time() <= $this->getEndTimeDate()) ? 1 : 0;
        } elseif($this->getStartTimeDate()){
            $this->active = (date('Ymd') <= date('Ymd', $this->getStartTimeDate())) ? 1 : 0;
        }
        return ($this->active) ? 1 : 0;
    }

    /**
     * is Event available for current userGroup
     *
     * @return bool
     */
    public function isInGroup() {
        foreach ($this->formatType as $formatType) {
            foreach (explode(',', $formatType->getFeGroups()) as $group) {
                return in_array((int)$group, $GLOBALS['TSFE']->fe_user->groupData['uid']);
            }
        }
        return false;
    }
    
}
