<?php
namespace Netfed\CcEvents\Service;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Session
 */
class Session implements \TYPO3\CMS\Core\SingletonInterface
{

    /**
     * checks if object is stored in the user's session
     * @param string $key
     * @return boolean
     */
    public function has($key) {
        $sessionData = $this->getFrontendUser()->getKey('ses', $key);
        return $sessionData ? true : false;
    }

    /**
     * Returns the object stored in the user's session
     * @param string $key
     * @return Object the stored object
     */
    public function get($key) {
        $sessionData = $this->getFrontendUser()->getKey('ses', $key);
        return $sessionData ?: [];
    }

    /**
     * Writes something to storage
     * @param string $key
     * @param string $value
     * @return	void
     */
    public function set($key, $value) {
        $this->getFrontendUser()->setKey('ses', $key, $value);
        $this->getFrontendUser()->storeSessionData();
    }

    /**
     * Writes a object to the session if the key is empty it used the classname
     * @param object $object
     * @param string $key
     * @return	void
     */
    public function storeObject($object, $key = null) {
        if(is_null($key)) $key = get_class($object);
        return $this->set($key, serialize($object));
    }

    /**
     * Read something from storage
     * @param string $key
     * @return	object
     */
    public function getObject($key) {
        return $this->has($key) ? unserialize($this->get($key)) : [];
    }

    /**
     * Cleans up the session: removes the stored object from the PHP session
     * @param string $key
     * @return	void
     */
    public function clean($key) {
        $this->getFrontendUser()->setKey('ses', $key, NULL);
        $this->getFrontendUser()->storeSessionData();
    }

    /**
     * Gets a frontend user which is taken from the global registry or as fallback from TSFE->fe_user.
     *
     * @return	\TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication current frontend user
     * @throws	\LogicException
     */
    protected function getFrontendUser() {
        if ($GLOBALS['TSFE']->fe_user) {
            return $GLOBALS['TSFE']->fe_user;
        }
        throw new \LogicException('No Frontend user found in session!');
    }

}
