<?php
namespace Netfed\CcEvents\Service;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/**
 * Localization helper which should be used to fetch localized labels.
 */
class LocalizationUtility extends \TYPO3\CMS\Extbase\Utility\LocalizationUtility
{

    /**
     * Force changing language
     *
     * @param int $uid
     * @param string $lang
     * @param string $extensionName
     *
     * @return void
     **/
    public static function forceLanguage($uid, $lang, $extensionName)
    {
        self::$languageKey = $lang;

        $GLOBALS['TSFE']->lang = $lang;
        $GLOBALS['TSFE']->linkVars = '&L=' . $uid;
        $GLOBALS['TSFE']->sys_language_uid = $uid;

        \TYPO3\CMS\Core\Utility\GeneralUtility::_GETset($uid, 'L');

        $GLOBALS['TSFE']->initFEuser();
        $GLOBALS['TSFE']->initTemplate();
        $GLOBALS['TSFE']->getConfigArray();
        $GLOBALS['TSFE']->settingLanguage();
        $GLOBALS['TSFE']->settingLocale();

        self::initializeLanguage($extensionName);
    }

    /**
     * @param  string $extensionName
     *
     * @return void
     **/
    public static function initializeLanguage($extensionName)
    {
        $locallangPathAndFilename = 'EXT:' . \TYPO3\CMS\Core\Utility\GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName) . '/' . self::$locallangPath . 'locallang.xlf';
        $renderCharset = TYPO3_MODE === 'FE' ? self::getTypoScriptFrontendController()->renderCharset : self::getLanguageService()->charSet;

        /** @var $languageFactory \TYPO3\CMS\Core\Localization\LocalizationFactory */
        $languageFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Localization\LocalizationFactory::class);

        self::$LOCAL_LANG[$extensionName] = $languageFactory->getParsedData($locallangPathAndFilename, self::$languageKey, $renderCharset);
        foreach (self::$alternativeLanguageKeys as $language) {
            $tempLL = $languageFactory->getParsedData($locallangPathAndFilename, $language, $renderCharset);
            if (self::$languageKey !== 'default' && isset($tempLL[$language])) {
                self::$LOCAL_LANG[$extensionName][$language] = $tempLL[$language];
            }
        }
        self::loadTypoScriptLabels($extensionName);
    }

}
