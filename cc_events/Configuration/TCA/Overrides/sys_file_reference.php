<?php
$temp_metacolumns = [
    'author' => [
        'exclude' => 1,
        'label' => 'Author',
        'config' => [
            'type' => 'input',
            'size' => 20,
            'eval' => 'trim'
        ],
    ],
];
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference',$temp_metacolumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_file_reference', 'author', '', 'after:title');