<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_groups',
        ],
        'searchFields' => 'title,text,start_date,end_date,city,price,hotel,dress_code,downloads,partner,registration_info',
        'iconfile' => 'EXT:cc_events/Resources/Public/Icons/tx_ccevents_domain_model_event.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, text, start_date, end_date, city, price, hotel, dress_code, downloads, partner, registration_info',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, text, start_date, end_date, city, price, hotel, dress_code, downloads, partner, format_type, registration_info, participants_group, number_of_participants, requirement, procedure, venue, venue_and_hotel, dates, termin_series, series, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime, fe_groups'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_ccevents_domain_model_event',
                'foreign_table_where' => 'AND tx_ccevents_domain_model_event.pid=###CURRENT_PID### AND tx_ccevents_domain_model_event.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'text' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.text',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'registration_info' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.registration_info',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'start_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.start_date',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'datetime, required',
                'default' => time()
            ],
        ],
        'end_date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.end_date',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'datetime',
                'default' => ''
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'price' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.price',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'hotel' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.hotel',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'dress_code' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.dress_code',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'downloads' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.downloads',
            'config' =>
                \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                    'downloads',
                    [
                        'appearance' => [
                            'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
                        ],
                        'foreign_types' => [
                            '0' => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                                'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                            ]
                        ],
                        'maxitems' => 99999
                    ]
                ),

        ],
        'partner' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.partner',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_ccevents_domain_model_partner',
                'minitems' => 0,
                'maxitems' => 2,
            ],
        ],
        'format_type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.format_types',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_ccevents_domain_model_formattype',
                'MM' => 'tx_ccevents_formattype_event_mm',
                'MM_opposite_field' => 'uid_foreign',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 1,
                'multiple' => 0,
            ],
        ],
        'fe_groups' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.fe_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'size' => 5,
                'maxitems' => 20,
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.any_login',
                        -2
                    ],
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.usergroups',
                        '--div--'
                    ]
                ],
                'exclusiveKeys' => '-1,-2',
                'foreign_table' => 'fe_groups',
                'foreign_table_where' => 'ORDER BY fe_groups.title'
            ]
        ],
        'componentmodules' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.componentmodules',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                    ['LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.component', 1],
                    ['LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.modules', 2],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'termin_series' => [
            'exclude' => true,
//            'displayCond' => 'FIELD:main_termin:=:0',
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.termin_series',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_ccevents_domain_model_event',
                'foreign_table_where' => 'tx_ccevents_domain_model_event.uid != ###REC_FIELD_uid###',
                'MM' => 'tx_ccevents_termin_series_mm',
                'MM_oppositeUsage' => [
                    'tx_ccevents_domain_model_event' => ['main_termin'],
                ],
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 4,
                'multiple' => 0,
                'default' => 0,
            ],
        ],
        'main_termin' => [
            'exclude' => true,
//            'displayCond' => 'FIELD:termin_series:=:0',
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.main_termin',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_ccevents_domain_model_event',
                'MM' => 'tx_ccevents_termin_series_mm',
                'MM_opposite_field' => 'uid_foreign',
                'MM_oppositeUsage' => [
                    'tx_ccevents_domain_model_event' => ['termin_series'],
                ],
                'items' => [
                    ['', 0]
                ],
                'size' => 1,
                'maxitems' => 1,
                'multiple' => 0,
                'default' => 0,
            ],
        ],
        'series' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.series',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'participants_group' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.participants_group',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'number_of_participants' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.number_of_participants',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'requirement' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.requirement',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'procedure' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.procedure',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'venue' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.venue',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'venue_and_hotel' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.venue_and_hotel',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'dates' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_event.dates',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
    ],
];
