<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration',
        'label' => 'title',
        'label_alt' => 'last_name, first_name, event',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title, anrede, last_name, first_name, email, telephone, mobile, company, function, address, zip, city, description, terms, event',
        'iconfile' => 'EXT:cc_events/Resources/Public/Icons/tx_ccevents_domain_model_registration.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, anrede, last_name, first_name, email, telephone, mobile, company, function, address, zip, city, description, terms, event',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, anrede, last_name, first_name, email, telephone, mobile, company, function, address, zip, city, description, terms, event, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_ccevents_domain_model_registration',
                'foreign_table_where' => 'AND tx_ccevents_domain_model_registration.pid=###CURRENT_PID### AND tx_ccevents_domain_model_registration.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'title' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'anrede' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.anrede',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'last_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.last_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'first_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.first_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'telephone' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.telephone',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'mobile' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.mobile',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ]
        ],
        'company' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.company',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'function' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.function',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'address' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.address',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'zip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.zip',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.description',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'terms' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.terms',
            'config' => [
                'type' => 'check',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'Disable',
            'config' => [
                'type' => 'check',
            ],
        ],
        'event' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.event',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_ccevents_domain_model_event',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'user' => [
            'exclude' => true,
            'label' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_ccevents_domain_model_registration.user',
            'config' => [
                'type' => 'input',
            ],
        ],
    ],
];
