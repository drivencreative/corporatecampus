plugin.tx_ccevents_events {
    view {
        templateRootPaths.0 = EXT:cc_events/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_ccevents_events.view.templateRootPath}
        partialRootPaths.0 = EXT:cc_events/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_ccevents_events.view.partialRootPath}
        layoutRootPaths.0 = EXT:cc_events/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_ccevents_events.view.layoutRootPath}
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
    settings {
        maxWidth = 460
    }
}
# Json url
api_tag = PAGE
api_tag {
    config {
        disableAllHeaderCode = 1
        debug = 0
        no_cache = 1
        additionalHeaders {
            10 {
                header = Content-Type: application/json
                replace = 1
            }
        }
    }
    typeNum = 19852604
    10 < tt_content.list.20.ccevents_events
}

plugin.tx_ccevents_form {
    view < plugin.tx_ccevents_events.view
    persistence {
        storagePid = 20
    }
    settings {
        view < plugin.tx_ccevents_form.view
        sender = info@corporate-publications.com
        receiver = dradisic@peak-sourcing.com
        subject = Corporate Campus: Anmeldung zur Veranstaltung
        css = EXT:cc_events/Resources/Public/CSS/mail.css
        # Terms page uid
        terms = 18
        # Registration Confirmation contact person
        contact = 27
    }
}
# Json url
api_form = PAGE
api_form {
    config {
        disableAllHeaderCode = 1
        debug = 0
        no_cache = 1
    }
    typeNum = 226688
    10 < tt_content.list.20.ccevents_form
}

# Module configuration
module.tx_ccevents_web_export {
    persistence < plugin.tx_ccevents_form.persistence
    view < plugin.tx_ccevents_form.view
    settings < plugin.tx_ccevents_form.settings
}

config.tx_extbase {
    persistence {
    # Enable this if you need the reference index to be updated
		# updateReferenceIndex = 1
        classes {
            Netfed\CcEvents\Domain\Model\FileReference {
                mapping {
                    tableName = sys_file_reference
                    columns {
                        uid_local.mapOnProperty = originalFileIdentifier
                    }
                }
            }
        }
    }
    objects {
        TYPO3\CMS\Extbase\Domain\Model\FileReference.className = Netfed\CcEvents\Domain\Model\FileReference
    }
}

plugin.tx_ccevents_form {
    _LOCAL_LANG.de {
        tx_ccevents_domain_model_registration.titlePlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.titlePlaceholder}
        tx_ccevents_domain_model_registration.lastNamePlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.lastNamePlaceholder}
        tx_ccevents_domain_model_registration.firstNamePlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.firstNamePlaceholder}
        tx_ccevents_domain_model_registration.emailPlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.emailPlaceholder}
        tx_ccevents_domain_model_registration.telephonePlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.telephonePlaceholder}
        tx_ccevents_domain_model_registration.mobilePlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.mobilePlaceholder}
        tx_ccevents_domain_model_registration.companyPlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.companyPlaceholder}
        tx_ccevents_domain_model_registration.functionPlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.functionPlaceholder}
        tx_ccevents_domain_model_registration.addressPlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.addressPlaceholder}
        tx_ccevents_domain_model_registration.zipPlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.zipPlaceholder}
        tx_ccevents_domain_model_registration.cityPlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.cityPlaceholder}
        tx_ccevents_domain_model_registration.descriptionPlaceholder = {$plugin.tx_ccevents_form._LOCAL_LANG.de.tx_ccevents_domain_model_registration.descriptionPlaceholder}
    }
}
