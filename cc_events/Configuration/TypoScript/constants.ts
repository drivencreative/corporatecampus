
plugin.tx_ccevents_events {
    view {
        # cat=plugin.tx_ccevents_events/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:cc_events/Resources/Private/Templates/
        # cat=plugin.tx_ccevents_events/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:cc_events/Resources/Private/Partials/
        # cat=plugin.tx_ccevents_events/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:cc_events/Resources/Private/Layouts/
    }
}
plugin.tx_ccevents_form {
    _LOCAL_LANG.de {
        # cat=plugin.tx_ccevents_form/lang; type=string; label=title field placeholder
        tx_ccevents_domain_model_registration.titlePlaceholder = Bitte geben Sie hier Ihren Titel an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=last name field placeholder
        tx_ccevents_domain_model_registration.lastNamePlaceholder = Bitte geben Sie hier Ihren Nachnamen an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=first name field placeholder
        tx_ccevents_domain_model_registration.firstNamePlaceholder = Bitte geben Sie hier Ihren Vornamen an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=email field placeholder
        tx_ccevents_domain_model_registration.emailPlaceholder = Bitte geben Sie hier Ihre E-Mail-Adresse an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=telephone field placeholder
        tx_ccevents_domain_model_registration.telephonePlaceholder = Bitte geben Sie hier Ihre Telefonnummer an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=mobile field placeholder
        tx_ccevents_domain_model_registration.mobilePlaceholder = Bitte geben Sie hier Ihre Mobilnummer an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=company field placeholder
        tx_ccevents_domain_model_registration.companyPlaceholder = Bitte geben Sie hier Ihr Unternehmen an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=function field placeholder
        tx_ccevents_domain_model_registration.functionPlaceholder = Bitte geben Sie hier Ihre Funktion an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=address field placeholder
        tx_ccevents_domain_model_registration.addressPlaceholder = Bitte geben Sie hier Ihre Adresse an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=zip field placeholder
        tx_ccevents_domain_model_registration.zipPlaceholder = Bitte geben Sie hier Ihre Postleitzahl an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=city field placeholder
        tx_ccevents_domain_model_registration.cityPlaceholder = Bitte geben Sie hier Ihre Stadt an

        # cat=plugin.tx_ccevents_form/lang; type=string; label=description field placeholder
        tx_ccevents_domain_model_registration.descriptionPlaceholder = Bitte nutzen Sie dieses Feld bei Bedarf für Ihre Anmerkungen
    }
}