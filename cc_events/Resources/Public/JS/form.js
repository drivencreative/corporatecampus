$('body').on({
    submit: function() {
        var form = $(this),
            data = form.serializeArray(),
            url = form.attr('action');
        $('#register').empty().addClass('loader');
        $.post(url, data, function(response) {
            $('.fancybox-slide').html(response);
            $('input').iCheck();
        });

        return false;
    }
}, '#form_contact_registration');

$('body').on({
    ifClicked: function() {
        if (this.checked) {
            $('#submitRegistration').attr('disabled', 'disabled').addClass('disabled');
        } else {
            $('#submitRegistration').removeAttr('disabled').removeClass('disabled');
        }
    }
}, '#form_contact_registration input');