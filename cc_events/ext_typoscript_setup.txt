config.tx_extbase {
    persistence{
        enableAutomaticCacheClearing = 1
        updateReferenceIndex = 0
        classes {
            Netfed\CcEvents\Domain\Model\Content {
                mapping {
                    tableName = tt_content
                    columns {
                        uid.mapOnProperty = uid
                        pid.mapOnProperty = pid
                        sorting.mapOnProperty = sorting
                        CType.mapOnProperty = contentType
                        header.mapOnProperty = header
                    }
                }
            }
            Netfed\CcEvents\Domain\Model\FileReference {
                mapping {
                    tableName = sys_file_reference
                    columns {
                        title.mapOnProperty = title
                        description.mapOnProperty = description
                    }
                }
            }
        }
    }
    objects {
        TYPO3\CMS\Extbase\Domain\Model\FileReference.className = Netfed\CcEvents\Domain\Model\FileReference
    }
}