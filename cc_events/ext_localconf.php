<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.CcEvents',
            'Events',
            [
                'Event' => 'list, show, download, filter, file, calendar'
            ],
            // non-cacheable actions
            [
                'Event' => 'list, show, download, filter, file, calendar'
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.CcEvents',
            'EventsSlider',
            [
                'Event' => 'listSlider, show'
            ],
            // non-cacheable actions
            [
                'Event' => ''
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.CcEvents',
            'FormatsReview',
            [
                'Event' => 'formatsReview, show'
            ],
            // non-cacheable actions
            [
                'Event' => ''
            ]
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.CcEvents',
            'Form',
            [
                'Registration' => 'index, process, confirmation, update'
            ],
            // non-cacheable actions
            [
                'Registration' => 'index, process, confirmation, update'
            ]
        );

        // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    events {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('cc_events') . 'Resources/Public/Icons/user_plugin_events.svg
                        title = LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_cc_events_domain_model_events
                        description = LLL:EXT:cc_events/Resources/Private/Language/locallang_db.xlf:tx_cc_events_domain_model_events.description
                        tt_content_defValues {
                            CType = list
                            list_type = ccevents_events
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
