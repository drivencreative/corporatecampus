<?php
namespace Netfed\CcEvents\Tests\Unit\Controller;

/**
 * Test case.
 */
class PartnerControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\CcEvents\Controller\PartnerController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\CcEvents\Controller\PartnerController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllPartnersFromRepositoryAndAssignsThemToView()
    {

        $allPartners = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $partnerRepository = $this->getMockBuilder(\Netfed\CcEvents\Domain\Repository\PartnerRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $partnerRepository->expects(self::once())->method('findAll')->will(self::returnValue($allPartners));
        $this->inject($this->subject, 'partnerRepository', $partnerRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('partners', $allPartners);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenPartnerToView()
    {
        $partner = new \Netfed\CcEvents\Domain\Model\Partner();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('partner', $partner);

        $this->subject->showAction($partner);
    }
}
