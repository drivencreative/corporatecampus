<?php
namespace Netfed\CcEvents\Tests\Unit\Controller;

/**
 * Test case.
 */
class FormatTypeControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\CcEvents\Controller\FormatTypeController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Netfed\CcEvents\Controller\FormatTypeController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllFormatTypesFromRepositoryAndAssignsThemToView()
    {

        $allFormatTypes = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $formatTypeRepository = $this->getMockBuilder(\Netfed\CcEvents\Domain\Repository\FormatTypeRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $formatTypeRepository->expects(self::once())->method('findAll')->will(self::returnValue($allFormatTypes));
        $this->inject($this->subject, 'formatTypeRepository', $formatTypeRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('formatTypes', $allFormatTypes);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }

    /**
     * @test
     */
    public function showActionAssignsTheGivenFormatTypeToView()
    {
        $formatType = new \Netfed\CcEvents\Domain\Model\FormatType();

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $this->inject($this->subject, 'view', $view);
        $view->expects(self::once())->method('assign')->with('formatType', $formatType);

        $this->subject->showAction($formatType);
    }
}
