<?php
namespace Netfed\CcEvents\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class PilarTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\CcEvents\Domain\Model\Pilar
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\CcEvents\Domain\Model\Pilar();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFormatsReturnsInitialValueForFormatType()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getFormats()
        );
    }

    /**
     * @test
     */
    public function setFormatsForObjectStorageContainingFormatTypeSetsFormats()
    {
        $format = new \Netfed\CcEvents\Domain\Model\FormatType();
        $objectStorageHoldingExactlyOneFormats = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneFormats->attach($format);
        $this->subject->setFormats($objectStorageHoldingExactlyOneFormats);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneFormats,
            'formats',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addFormatToObjectStorageHoldingFormats()
    {
        $format = new \Netfed\CcEvents\Domain\Model\FormatType();
        $formatsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $formatsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($format));
        $this->inject($this->subject, 'formats', $formatsObjectStorageMock);

        $this->subject->addFormat($format);
    }

    /**
     * @test
     */
    public function removeFormatFromObjectStorageHoldingFormats()
    {
        $format = new \Netfed\CcEvents\Domain\Model\FormatType();
        $formatsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $formatsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($format));
        $this->inject($this->subject, 'formats', $formatsObjectStorageMock);

        $this->subject->removeFormat($format);
    }
}
