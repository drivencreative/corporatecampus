<?php
namespace Netfed\CcEvents\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class EventTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\CcEvents\Domain\Model\Event
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\CcEvents\Domain\Model\Event();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText()
        );
    }

    /**
     * @test
     */
    public function setTextForStringSetsText()
    {
        $this->subject->setText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStartDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getStartDate()
        );
    }

    /**
     * @test
     */
    public function setStartDateForDateTimeSetsStartDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setStartDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'startDate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEndDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getEndDate()
        );
    }

    /**
     * @test
     */
    public function setEndDateForDateTimeSetsEndDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setEndDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'endDate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity()
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'city',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPriceReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getPrice()
        );
    }

    /**
     * @test
     */
    public function setPriceForFloatSetsPrice()
    {
        $this->subject->setPrice(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'price',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getHotelReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHotel()
        );
    }

    /**
     * @test
     */
    public function setHotelForStringSetsHotel()
    {
        $this->subject->setHotel('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'hotel',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDressCodeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDressCode()
        );
    }

    /**
     * @test
     */
    public function setDressCodeForStringSetsDressCode()
    {
        $this->subject->setDressCode('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'dressCode',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDownloadsReturnsInitialValueForFileReference()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getDownloads()
        );
    }

    /**
     * @test
     */
    public function setDownloadsForFileReferenceSetsDownloads()
    {
        $download = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $objectStorageHoldingExactlyOneDownloads = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneDownloads->attach($download);
        $this->subject->setDownloads($objectStorageHoldingExactlyOneDownloads);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneDownloads,
            'downloads',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addDownloadToObjectStorageHoldingDownloads()
    {
        $download = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $downloadsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $downloadsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($download));
        $this->inject($this->subject, 'downloads', $downloadsObjectStorageMock);

        $this->subject->addDownload($download);
    }

    /**
     * @test
     */
    public function removeDownloadFromObjectStorageHoldingDownloads()
    {
        $download = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $downloadsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $downloadsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($download));
        $this->inject($this->subject, 'downloads', $downloadsObjectStorageMock);

        $this->subject->removeDownload($download);
    }

    /**
     * @test
     */
    public function getComponentmodulesReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getComponentmodules()
        );
    }

    /**
     * @test
     */
    public function setComponentmodulesForIntSetsComponentmodules()
    {
        $this->subject->setComponentmodules(12);

        self::assertAttributeEquals(
            12,
            'componentmodules',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPartnerReturnsInitialValueForPartner()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getPartner()
        );
    }

    /**
     * @test
     */
    public function setPartnerForObjectStorageContainingPartnerSetsPartner()
    {
        $partner = new \Netfed\CcEvents\Domain\Model\Partner();
        $objectStorageHoldingExactlyOnePartner = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOnePartner->attach($partner);
        $this->subject->setPartner($objectStorageHoldingExactlyOnePartner);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOnePartner,
            'partner',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addPartnerToObjectStorageHoldingPartner()
    {
        $partner = new \Netfed\CcEvents\Domain\Model\Partner();
        $partnerObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $partnerObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($partner));
        $this->inject($this->subject, 'partner', $partnerObjectStorageMock);

        $this->subject->addPartner($partner);
    }

    /**
     * @test
     */
    public function removePartnerFromObjectStorageHoldingPartner()
    {
        $partner = new \Netfed\CcEvents\Domain\Model\Partner();
        $partnerObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $partnerObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($partner));
        $this->inject($this->subject, 'partner', $partnerObjectStorageMock);

        $this->subject->removePartner($partner);
    }
}
