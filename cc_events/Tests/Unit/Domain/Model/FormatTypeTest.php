<?php
namespace Netfed\CcEvents\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class FormatTypeTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Netfed\CcEvents\Domain\Model\FormatType
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Netfed\CcEvents\Domain\Model\FormatType();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEventReturnsInitialValueForEvent()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getEvent()
        );
    }

    /**
     * @test
     */
    public function setEventForObjectStorageContainingEventSetsEvent()
    {
        $event = new \Netfed\CcEvents\Domain\Model\Event();
        $objectStorageHoldingExactlyOneEvent = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneEvent->attach($event);
        $this->subject->setEvent($objectStorageHoldingExactlyOneEvent);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneEvent,
            'event',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addEventToObjectStorageHoldingEvent()
    {
        $event = new \Netfed\CcEvents\Domain\Model\Event();
        $eventObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $eventObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($event));
        $this->inject($this->subject, 'event', $eventObjectStorageMock);

        $this->subject->addEvent($event);
    }

    /**
     * @test
     */
    public function removeEventFromObjectStorageHoldingEvent()
    {
        $event = new \Netfed\CcEvents\Domain\Model\Event();
        $eventObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $eventObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($event));
        $this->inject($this->subject, 'event', $eventObjectStorageMock);

        $this->subject->removeEvent($event);
    }
}
