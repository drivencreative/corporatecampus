<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        $pluginSignature = str_replace('_', '', 'cc_events');

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.CcEvents',
            'Events',
            'Events'
        );
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature . '_events'] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature . '_events', 'FILE:EXT:cc_events/Configuration/FlexForms/flexform_events.xml');

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.CcEvents',
            'EventsSlider',
            'Events Slider'
        );
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature . '_eventsslider'] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature . '_eventsslider', 'FILE:EXT:cc_events/Configuration/FlexForms/flexform_events_slider.xml');

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.CcEvents',
            'FormatsReview',
            'Formats Review'
        );
        $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature . '_formatsreview'] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature . '_formatsreview', 'FILE:EXT:cc_events/Configuration/FlexForms/flexform_formats_review.xml');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('cc_events', 'Configuration/TypoScript', 'Corporate Campus');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ccevents_domain_model_event', 'EXT:cc_events/Resources/Private/Language/locallang_csh_tx_ccevents_domain_model_event.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ccevents_domain_model_event');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ccevents_domain_model_pilar', 'EXT:cc_events/Resources/Private/Language/locallang_csh_tx_ccevents_domain_model_pilar.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ccevents_domain_model_pilar');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ccevents_domain_model_partner', 'EXT:cc_events/Resources/Private/Language/locallang_csh_tx_ccevents_domain_model_partner.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ccevents_domain_model_partner');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ccevents_domain_model_formattype', 'EXT:cc_events/Resources/Private/Language/locallang_csh_tx_ccevents_domain_model_formattype.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ccevents_domain_model_formattype');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_ccevents_domain_model_registration', 'EXT:cc_events/Resources/Private/Language/locallang_csh_tx_ccevents_domain_model_registration.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_ccevents_domain_model_registration');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'cc_events',
            'tx_ccevents_domain_model_event'
        );

        if (TYPO3_MODE === 'BE') {
            /**
             * Registers a Backend Module
             */
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Netfed.CcEvents',
                'web',	    // Make module a submodule of 'web'
                'export',	// Submodule key
                '',					// Position
                array(
                    'Export' => 'index, process',
                ),
                array(
                    'access' => 'user,group',
                    'labels' => 'LLL:EXT:cc_events/Resources/Private/Language/locallang_export.xlf',
                )
            );
        }

    }
);
