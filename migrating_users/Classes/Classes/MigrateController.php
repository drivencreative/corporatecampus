<?php
namespace Netfed\MigratingUsers\Controller;
/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use function GuzzleHttp\default_ca_bundle;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class MigrateController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \SQLite3
     */
    protected $database;

    /**
     * action initialize
     *
     * @return void
     */
    public function initializeAction()
    {
        $this->database = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('SQLite3', '/var/www/cc/public_html/storage.sqlite');
        $this->database->enableExceptions(true);

    }

    /**
     * action index
     *
     * @return void
     */
    public function indexAction()
    {
//        DebuggerUtility::var_dump($this->database);die;

    }

    /**
     * action process
     *
     * @return array
     */
    public function processAction()
    {
        $table = $this->request->getArguments();
//        DebuggerUtility::var_dump($this->request->getArguments());die;
        switch ($table['migrate']['table']) {
        case 'fe_users':
            $status = $this->migrate(
                '
                SELECT u.id as uid,u.first_name,u.email as username, u.last_name,u.email,u.function,u.tel as telephone,u.company,u.city,u.plz as zip,u.street as address,u.fax,u.handy as mobile, GROUP_CONCAT(g.group_id) AS usergroup 
                FROM auth_user u
                LEFT JOIN auth_membership g ON g.user_id = u.id
                GROUP BY u.id
                ', 'fe_users'
            );
            break;
        case 'fe_groups':
            $status = $this->migrate(
                '
                SELECT u.id as uid,u.description,u.role as title
                FROM auth_group u
                ',
                'fe_groups'
            );
            break;
        default:
            echo '';
    }

        $this->view->assign('status', $status);
    }

    /**
     * @param $announces
     * @param $table
     * @return mixed
     */
    public function insert($announces,$table)
    {
        if(!empty($announces)) {
            $status = $GLOBALS['TYPO3_DB']->exec_INSERTmultipleRows(
                $table,
                array_keys($announces[0]),
                $announces
            );
        }

        return $status;

    }

    /**
     * @param string $sql
     * @param $table
     * @return mixed
     */
    private function migrate($sql,$table)
    {
        $res = [];
        $results = $this->database->query($sql);

        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            $res[] = $row;
        }
        $status = $this->insert($res,$table);
        return $status;
    }
}