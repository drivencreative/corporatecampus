<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('migrating_users', 'Configuration/TypoScript', 'Migrating Users');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_migratingusers_domain_model_user', 'EXT:migrating_users/Resources/Private/Language/locallang_csh_tx_migratingusers_domain_model_user.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_migratingusers_domain_model_user');

        if (TYPO3_MODE === 'BE') {
            /**
             * Registers a Backend Module
             */
            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Netfed.MigratingUsers',
                'web',	    // Make module a submodule of 'web'
                'migrate',	// Submodule key
                '',					// Position
                array(
                    'Migrate' => 'index, process',
                ),
                array(
                    'access' => 'user,group',
                    'labels' => 'LLL:EXT:migrating_users/Resources/Private/Language/locallang_db.xlf',
                )
            );
        }
    }
);
