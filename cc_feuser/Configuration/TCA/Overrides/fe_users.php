<?php

$fe_user = [
    'anrede' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:cc_feuser/Resources/Private/Language/locallang.xlf:user.anrede',
        'config' => [
            'type' => 'radio',
            'items' => [
                [ 'LLL:EXT:cc_feuser/Resources/Private/Language/locallang.xlf:user.anrede_frau', 1 ],

                [ 'LLL:EXT:cc_feuser/Resources/Private/Language/locallang.xlf:user.anrede_herr', 2 ],
            ],
        ],
    ],
    'function' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:cc_feuser/Resources/Private/Language/locallang_db.xlf:user.function',
        'config' => [
            'type' => 'input',
            'size' => 20,
        ],
    ],
    'mobile' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:cc_feuser/Resources/Private/Language/locallang_db.xlf:user.mobile',
        'config' => [
            'type' => 'input',
            'size' => 20,
            'eval' => 'trim'
        ],
    ],
    'telephone' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:cc_feuser/Resources/Private/Language/locallang_db.xlf:user.telephone',
        'config' => [
            'type' => 'input',
            'size' => 20,
            'eval' => 'trim'
        ],
    ],
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $fe_user, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'anrede', '', 'before:name');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'function', '', 'after:telephone');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('fe_users', 'mobile', '', 'after:telephone');