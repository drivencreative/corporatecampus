
plugin.tx_ccfeuser_feuser {
    view {
        # cat=plugin.tx_ccfeuser_feuser/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:cc_feuser/Resources/Private/Templates/
        # cat=plugin.tx_ccfeuser_feuser/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:cc_feuser/Resources/Private/Partials/
        # cat=plugin.tx_ccfeuser_feuser/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:cc_feuser/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_ccfeuser_feuser//a; type=string; label=Default storage PID
        storagePid =
    }
}
