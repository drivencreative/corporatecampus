<?php
namespace Netfed\CcFeuser\Controller;

/***
 *
 * This file is part of the "Corporate Campus Feuser" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use TYPO3\CMS\Core\Crypto\Random;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * UserController
 */
class FrontendLoginController extends \TYPO3\CMS\Felogin\Controller\FrontendLoginController
{

    /**
     * FrontendUserRepository
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * Generate link with typolink function
     *
     * @param string $label Linktext
     * @param array $piVars Link vars
     * @param bool $returnUrl TRUE: returns only url  FALSE (default) returns the link)
     * @return string Link or url
     */
    protected function getPageLink($label, $piVars, $returnUrl = false)
    {
        $additionalParams = '';
        if (!empty($piVars)) {
            foreach ($piVars as $key => $val) {
                $additionalParams .= '&' . $key . '=' . $val;
            }
        }
        // Should GETvars be preserved?
        if ($this->conf['preserveGETvars']) {
            $additionalParams .= $this->getPreserveGetVars();
        }

        $this->conf['linkConfig.']['parameter'] = $this->conf['linkConfig.']['parameter'] ?: $this->frontendController->id;
//        DebuggerUtility::var_dump($this->conf['linkConfig.']['parameter']);
        if ($additionalParams) {
            parse_str($additionalParams, $paramsArray);
            $this->conf['linkConfig.']['additionalParams'] = $additionalParams;
            $this->conf['linkConfig.']['parameter'] = $paramsArray['tx_felogin_pi1']['forgot'] ? $this->conf['linkConfig.']['parameter'] : ($this->conf['redirectPageLogin'] ?: $this->frontendController->id);
        }
//        DebuggerUtility::var_dump($this->conf['linkConfig.']);
        if ($returnUrl) {
            return htmlspecialchars($this->cObj->typoLink_URL($this->conf['linkConfig.']));
        } else {
            return $this->cObj->typoLink($label, $this->conf['linkConfig.']);
        }
    }

    /**
     * Generates a hashed link and send it with email
     *
     * @param array $user Contains user data
     * @return string Empty string with success, error message with no success
     */
    protected function generateAndSendHash($user)
    {
        $hours = (int)$this->conf['forgotLinkHashValidTime'] > 0 ? (int)$this->conf['forgotLinkHashValidTime'] : 24;
        $validEnd = time() + 3600 * $hours;
        $validEndString = date($this->conf['dateFormat'], $validEnd);
        $hash = md5(GeneralUtility::makeInstance(Random::class)->generateRandomBytes(64));
        $randHash = $validEnd . '|' . $hash;
        $randHashDB = $validEnd . '|' . md5($hash);

        // Write hash to DB
        $userTable = $this->frontendController->fe_user->user_table;
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($userTable);
        $queryBuilder->getRestrictions()->removeAll();
        $queryBuilder->update($userTable)
            ->set('felogin_forgotHash', $randHashDB)
            ->where(
                $queryBuilder->expr()->eq(
                    'uid',
                    $queryBuilder->createNamedParameter($user['uid'], \PDO::PARAM_INT)
                )
            )
            ->execute();

        // Send hashlink to user
        $this->conf['linkPrefix'] = -1;
        $isAbsRefPrefix = !empty($this->frontendController->absRefPrefix);
        $isBaseURL = !empty($this->frontendController->baseUrl);
        $isFeloginBaseURL = !empty($this->conf['feloginBaseURL']);
        $link = $this->pi_getPageLink($this->frontendController->id, '', [
            rawurlencode($this->prefixId . '[user]') => $user['uid'],
            rawurlencode($this->prefixId . '[forgothash]') => $randHash
        ]);
        // Prefix link if necessary
        if ($isFeloginBaseURL) {
            // First priority, use specific base URL
            // "absRefPrefix" must be removed first, otherwise URL will be prepended twice
            if ($isAbsRefPrefix) {
                $link = substr($link, strlen($this->frontendController->absRefPrefix));
            }
            $link = $this->conf['feloginBaseURL'] . $link;
        } elseif ($isAbsRefPrefix) {
            // Second priority
            // absRefPrefix must not necessarily contain a hostname and URL scheme, so add it if needed
            $link = GeneralUtility::locationHeaderUrl($link);
        } elseif ($isBaseURL) {
            // Third priority
            // Add the global base URL to the link
            $link = $this->frontendController->baseUrlWrap($link);
        } else {
            // No prefix is set, return the error
            return $this->pi_getLL('ll_change_password_nolinkprefix_message');
        }
        $fullUser = $this->getDBStuff($user['uid'], 'fe_users');
        $dear = sprintf(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('ll_dear_'. $fullUser['anrede'], 'site_package'));
        $salutation = sprintf(\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('ll_salutation_'. $fullUser['anrede'], 'site_package'));
        $text = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('ll_forgot_validate_reset_password', 'site_package');
        $msg = sprintf($text, $dear, $salutation, $fullUser['first_name'], $fullUser['last_name'], $link, $validEndString);

        // Add hook for extra processing of mail message
        if (
            isset($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['forgotPasswordMail'])
            && is_array($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['forgotPasswordMail'])
        ) {
            $params = [
                'message' => &$msg,
                'user' => &$user
            ];
            foreach ($GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['forgotPasswordMail'] as $reference) {
                if ($reference) {
                    GeneralUtility::callUserFunction($reference, $params, $this);
                }
            }
        }
        if ($user['email']) {
            $this->sendEmail(
                $user['email'],
                $this->settings['sender'],
                \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('ll_password_forgot_mail_subject', 'site_package'),
                $msg
            );
        }

        return '';
    }

    /**
     * Send mail
     *
     * @param string $receiver
     * @param string $sender
     * @param string $subject
     * @param string $messageBody
     * @param mixed $attachment
     *
     * @return bool
     */
    protected function sendEmail($receiver, $sender, $subject, $messageBody, $attachment = NULL) {
        /** @var $message \TYPO3\CMS\Core\Mail\MailMessage **/
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $message->setTo($receiver)->setFrom($sender)->setSubject($subject);
        $message->setBody($messageBody, 'text/html');

        if ($attachment) {
            $attachment = is_array($attachment) ? $attachment : [$attachment];
            foreach ($attachment as $attach) {
                $message->attach(\Swift_Attachment::fromPath($attach['url'] ?: $attach));
            }
        }

        $message->send();

        return $message->isSent();
    }

    /**
     * @param $fe_user
     * @param $table
     * @return null
     *
     */
    function getDBStuff($fe_user, $table) {
        $value = null ;
        $res = $GLOBALS['TYPO3_DB']->exec_SELECTquery(
            '*' ,
            $table ,
            'uid = ' . $fe_user
        );
        if($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
            $value = $row ;
        }
        $GLOBALS['TYPO3_DB']->sql_free_result($res);
        return $value;
    }

}
