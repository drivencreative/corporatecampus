<?php
namespace Netfed\CcFeuser\Controller;

/***
 *
 * This file is part of the "Corporate Campus Feuser" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * UserController
 */
class UserController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * userRepository
     *
     * @var \Netfed\CcFeuser\Domain\Repository\UserRepository
     * @inject
     */
    protected $userRepository = null;

    /**
     * @var array
     */
    protected $configuration = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * Initialize Action
     */
    public function initializeAction()
    {
        if ($this->configuration = reset($this->arguments)) {
            $this->configuration = $this->configuration->getPropertyMappingConfiguration();
        }
    }

    /**
     * action show
     *
     * @param \Netfed\CcFeuser\Domain\Model\User $user
     * @return void
     */
    public function showAction(\Netfed\CcFeuser\Domain\Model\User $user)
    {
        $this->view->assign('user', $user);
    }

    /**
     * action edit
     *
     * @return void
     */
    public function editAction()
    {

        $this->view->assign('request', $this->request->getArguments());
        $this->view->assign('user', $this->userRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']));
    }



    /**
     * action update
     *
     * @param \Netfed\CcFeuser\Domain\Model\User $user
     * @return void
     */
    public function updateAction(\Netfed\CcFeuser\Domain\Model\User $user)
    {
        //\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->userRepository);die;
        if ($user->getUid() == $GLOBALS['TSFE']->fe_user->user['uid']) {
            $this->userRepository->update($user);
            $this->persistenceManager->persistAll();

            $this->addFlashMessage('The object was updated.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        }

        $this->redirect('edit');
    }

    /**
     * action passwordupdate
     *
     * @param \Netfed\CcFeuser\Domain\Model\User $user
     * @ignorevalidation $user
     * @return void
     */
    public function passwordUpdateAction(\Netfed\CcFeuser\Domain\Model\User $user)
    {
        $request = $this->request->getArguments();
        if (!$request['user']['password'] || ($request['user']['password'] != $request['user']['passwordRepeat'])) {
            $this->forward('edit', null, null, ['error' => ['password' => 1]]);
        }

        if ($request['user']['password']) {
            $this->hashPassword($user);
        }

        if ($user->getUid() == $GLOBALS['TSFE']->fe_user->user['uid']) {
            $this->userRepository->update($user);

            $this->addFlashMessage('The object was updated.', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        }
        $this->redirect('edit');



    }

    /**
     * Hash a password from $user->getPassword()
     *
     * @param \Netfed\CcFeuser\Domain\Model\User $user
     * @return void
     */
    protected function hashPassword(&$user) {
        if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('saltedpasswords')) {
            if (\TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility::isUsageEnabled('FE')) {
                $objInstanceSaltedPw = \TYPO3\CMS\Saltedpasswords\Salt\SaltFactory::getSaltingInstance();
                $user->setPassword($objInstanceSaltedPw->getHashedPassword($user->getPassword()));
            }
        }
    }

}
