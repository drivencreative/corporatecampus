<?php

namespace Netfed\CcFeuser\ViewHelpers;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class UserLoginViewHelper
 *
 * @package Netfed\CcFeuser\ViewHelpers;
 */
class UserLoginViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    public function render()
    {
        if ($GLOBALS['TSFE']->fe_user->user['first_name'] || $GLOBALS['TSFE']->fe_user->user['last_name']) {
            $firstname = $GLOBALS['TSFE']->fe_user->user['first_name'];
            $lastname = $GLOBALS['TSFE']->fe_user->user['last_name'];
            return ($firstname . ' ' . $lastname);
        } else {
            return $GLOBALS['TSFE']->fe_user->user['username'];
        }
    }

}
