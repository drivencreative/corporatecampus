<?php

namespace Netfed\CcFeuser\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class IsForgotPasswordViewHelper
 *
 * @package Netfed\CcFeuser\ViewHelpers;
 */
class IsForgotPasswordViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{
    /**
     * @return bool
     */
    public function render()
    {
        return (GeneralUtility::_GP('tx_felogin_pi1')['forgot'] || ($GLOBALS['TSFE']->id == 31)) ? true : false;
    }
}