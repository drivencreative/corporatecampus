<?php
namespace Netfed\CcFeuser\Domain\Model;

/***
 *
 * This file is part of the "Corporate Campus Feuser" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * User
 */
class User extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{
    /**
     * Anrede 1 or 2 for mr or mrs
     *
     * @var integer
     * @validate NotEmpty
     */
    protected $anrede;

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $firstName = '';

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $lastName = '';

    /**
     * @var string
     * @validate NotEmpty,EmailAddress
     */
    protected $email = '';

    /**
     * @var string
     * @validate NotEmpty
     */
    protected $telephone = '';

    /**
     * passwordRepeat
     *
     * @var string
     */
    protected $passwordRepeat = '';

    /**
     * mobile
     * @var string
     */
    protected $mobile = NULL;

    /**
     * Getter for anrede
     *
     * @return integer
     */
    public function getAnrede()
    {
        return $this->anrede;
    }

    /**
     * Setter for anrede
     *
     * @param integer $anrede
     * @return void
     */
    public function setAnrede($anrede)
    {
        $this->anrede = $anrede;
    }

    /**
     * Function
     *
     * @var string
     */
    protected $function;

    /**
     * Getter for function
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * Setter for function
     *
     * @param string $function
     * @return void
     */
    public function setFunction($function)
    {
        $this->function = $function;
    }

    /**
     * Returns the passwordRepeat value
     *
     * @return string
     * @api
     */
    public function getPasswordRepeat()
    {
        return $this->passwordRepeat;
    }

    /**
     * Setter passwordRepeat
     *
     * @param string $passwordRepeat
     * @return void
     */
    public function setPasswordRepeat($passwordRepeat)
    {
        $this->passwordRepeat = $passwordRepeat;
    }

    /**
     * Returns the mobile
     *
     * @return string $mobile
     */
    public function getMobile() {
        return $this->mobile;
    }

    /**
     * Setter mobile
     *
     * @param string $mobile
     * @return void
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

}
