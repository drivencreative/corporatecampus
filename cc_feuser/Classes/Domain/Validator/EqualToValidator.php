<?php
namespace Netfed\CcFeuser\Domain\Model\Validator;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Petar Colovic <petar.c@deepscreen.ch>, Deepscreen
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Validator for equal to values.
 */
class EqualToValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator
{

    /**
     * @var array
     */
    protected $supportedOptions = array(
        'firstProperty' => array('', 'The first property which should be compared with the second', 'string'),
        'secondProperty' => array('', 'The second property which should be compared with the first', 'string')
    );

    /**
     * @var \TYPO3\CMS\Extbase\Reflection\ReflectionService
     * @inject
     */
    protected $reflectionService;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Injects the object manager
     *
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     * @return void
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager) {
        $this->objectManager = $objectManager;
        $this->arguments = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Mvc\\Controller\\Arguments');
    }

    /**
     * Checks if the given first property value is equal to the second property value
     *
     * @param \TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject $object
     * @return bool TRUE, if the firstProperty is equal to the second, FALSE if not
     * @throws \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException
     */
    public function isValid($object) {
        $result = TRUE;
        $firstProperty = $this->options['firstProperty'];
        $secondProperty = $this->options['secondProperty'];

        if (empty($object) || $object === NULL) {
            throw new \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException('No domainObject given.', 1238107096);
        }
        if (!($object instanceof \TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject)) {
            throw new \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException('The domainObject is not instance of "\TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject".', 1238107097);
        }
        if (!property_exists($object, $firstProperty)) {
            throw new \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException('The first property: "' . $firstProperty . '" doest not exist in domainObject of type: "' . $object . '".', 1238107098);
        }
        if (!method_exists($object, 'get' . $firstProperty)) {
            throw new \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException('The getter for second property: "' . $firstProperty . '" doest not exist in domainObject of type: "' . $object . '".', 1238107099);
        }
        if (!property_exists($object, $secondProperty)) {
            throw new \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException('The second property: "' . $secondProperty . '" doest not exist in domainObject of type: "' . $object . '".', 1238107499);
        }
        if (!method_exists($object, 'get' . $secondProperty)) {
            throw new \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException('The getter for second property: "' . $secondProperty . '" doest not exist in domainObject of type: "' . $object . '".', 1238137099);
        }

        $firstValue = call_user_func_array(array($object, 'get' . $firstProperty), array());
        $secondValue = call_user_func_array(array($object, 'get' . $secondProperty), array());

        if (($object->_isDirty($firstProperty) || $object->_isDirty($secondProperty)) && $firstValue != $secondValue) {
            /** @var \TYPO3\CMS\Extbase\Validation\Error $error */
            $error = new \TYPO3\CMS\Extbase\Validation\Error($firstProperty . ' not equal to ' . $secondProperty, 1379662580);
            $this->result->forProperty($firstProperty)->addError($error);
            $result = FALSE;
        }

        return $result;
    }

}
