<?php
namespace Netfed\CcFeuser\Domain\Repository;

/***
 *
 * This file is part of the "Corporate Campus Feuser" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Users
 */
class UserRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    }
