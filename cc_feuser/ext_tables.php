<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Netfed.CcFeuser',
            'Feuser',
            'Feuser'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('cc_feuser', 'Configuration/TypoScript', 'Corporate Campus Feuser');

    }
);
