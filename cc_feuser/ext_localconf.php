<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Netfed.CcFeuser',
            'Feuser',
            [
                'User' => 'edit, update, passwordUpdate'
            ],
            // non-cacheable actions
            [
                'User' => 'edit, update, passwordUpdate'
            ]
        );

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        feuser {
                            icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('cc_feuser') . 'Resources/Public/Icons/user_plugin_feuser.svg
                            title = LLL:EXT:cc_feuser/Resources/Private/Language/locallang_db.xlf:tx_cc_feuser_domain_model_feuser
                            description = LLL:EXT:cc_feuser/Resources/Private/Language/locallang_db.xlf:tx_cc_feuser_domain_model_feuser.description
                            tt_content_defValues {
                                CType = list
                                list_type = ccfeuser_feuser
                            }
                        }
                    }
                    show = *
                }
           }'
        );
    }
);

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects']['TYPO3\\CMS\\Felogin\\Controller\\FrontendLoginController'] = ['className' => 'Netfed\\CcFeuser\\Controller\\FrontendLoginController'];