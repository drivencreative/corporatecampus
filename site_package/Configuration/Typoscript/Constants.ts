site_package{
    page {
        # cat=site_package/Page/100; type=int+; label= root:PID Root.
        rootPid = 1

        # cat=site_package/Page/110; type=int+; label= loginRoot:Login PID Root.
        rootPidLogin = 11

        # cat=site_package/Page/120; type=int+; label= rightNavPid:PID for Right Navigation.
        rightNavPid = 12

        # cat=site_package/Page/130; type=int+; label= footerNavPid:PID for Footer Navigation.
        footerNavPid = 16

        # cat=site_package/Page/140; type=int+; label= editProfilePid:PID for Profile Navigation.
        editProfilePid = 8

        # cat=site_package/Page/150; type=int+; label= loginPageFormPid:PID for Login Page.
        loginPageFormPid = 31

        # cat=site_package/Page/160; type=int+; label= usersPid:PID for Users storage.
        usersPid = 10
    }
}
