page = PAGE
page.10 < styles.content.get
page < temp.mask.page
page {
    10 {
        file.stdWrap.cObject.default.value = EXT:site_package/Resources/Private/Templates/Default.html
        file.stdWrap.cObject.404.value = EXT:site_package/Resources/Private/Templates/404.html

        layoutRootPaths {
            10 = EXT:site_package/Resources/Private/Layouts
        }
        partialRootPaths {
            10 = EXT:site_package/Resources/Private/Partials
        }
    }

    typeNum = 0
    shortcutIcon = fileadmin/user_upload/favicon-16x16.png

    includeCSS {
        font = https://fonts.googleapis.com/css?family=Adamina
        font.media = all
        styles = EXT:site_package/Resources/Public/css/styles.all.min.css
        additional = EXT:site_package/Resources/Public/css/additional.css
    }
    includeJS {
        globals = EXT:site_package/Resources/Public/js/globals.js
        scriptsHead = EXT:site_package/Resources/Public/js/scripts.head.all.min.js
    }

    includeJSFooter {
        scripts = EXT:site_package/Resources/Public/js/scripts.all.min.js
        iCheckDebuggerDestroy = EXT:site_package/Resources/Public/js/iCheckDebuggerDestroy.js
        check = EXT:cc_events/Resources/Public/JS/form.js
    }
}
# Remove empty p tags
tt_content.stdWrap.dataWrap >

# blockquote remove default styles
tt_content.text.20.parseFunc.externalBlocks.blockquote.callRecursive.tagStdWrap.HTMLparser.tags.blockquote.fixAttrib.style.unset=1
lib.parseFunc_RTE.externalBlocks.blockquote.callRecursive.tagStdWrap.HTMLparser.tags.blockquote.overrideAttribs
lib.content0 < styles.content.get
lib.content0.select.where = colPos=0
lib.content1 < styles.content.get
lib.content1.select.where = colPos=1

# Remove empty p tags
lib.parseFunc_RTE.nonTypoTagStdWrap.encapsLines >

#Remove default wrapping
#tt_content.stdWrap.innerWrap >
tt_content.stdWrap.innerWrap.cObject.default >

config {
    admPanel = 0
    concatenateJsAndCss = 1

    absRefPrefix = /
    tx_realurl_enable = 1
    contentObjectExceptionHandler = 0
}
# Disable global indexing, bind on typeNum!
page.bodyTag >
page.bodyTagCObject = TEXT
page.bodyTagCObject {
    field = uid
    wrap = <body class="page-|">
}