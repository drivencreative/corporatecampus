lib.language = HMENU
lib.language {
    special = language
    special.value = 0,1
    wrap = <ul class="nav navbar-nav">|</ul>
    1 = TMENU
    1 {
        NO = 1
        #NO.ATagParams = class="languageSelection"
        NO.stdWrap.override = Deutsch || English
        NO.linkWrap = <li>|</li>

        CUR = 1
        CUR.ATagParams = style="display: none;"
        CUR.linkWrap = <li>|</li>

        ACT < .CUR
        ACT = 1
    }
}


lib.rootPid = TEXT
lib.rootPid.value = {$site_package.page.rootPid}

lib.rootPidLogin = TEXT
lib.rootPidLogin.value = {$site_package.page.rootPidLogin}

lib.rightNavPid = TEXT
lib.rightNavPid.value = {$site_package.page.rightNavPid}

lib.footerNavPid = TEXT
lib.footerNavPid.value = {$site_package.page.footerNavPid}

lib.editProfilePid = TEXT
lib.editProfilePid.value = {$site_package.page.editProfilePid}

lib.loginPageFormPid = TEXT
lib.loginPageFormPid.value = {$site_package.page.loginPageFormPid}

lib.headerText = TEXT
lib.headerText.value = <p>DIALOG-PLATTFORM UND THINK TANK DER DZ BANK GRUPPE</p>

lib.felogin < plugin.tx_felogin_pi1
lib.felogin  {
    templateFile = EXT:site_package/Resources/Private/Templates/ext/felogin/Resources/Private/Templates/FrontendLogin.html


}
[globalVar = TSFE:id = {$site_package.page.loginPageFormPid}]
lib.felogin >
[global]

lib.loggedInUser = USER_INT
lib.loggedInUser.userFunc = Netfed\CcFeuser\ViewHelpers\UserLoginViewHelper->render
