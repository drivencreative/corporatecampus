page{
    headerData{
        # Canonical Tag
        20 = TEXT
        20 {
            typolink {
                parameter.data = TSFE:id
                useCacheHash = 1
                addQueryString = 1
                addQueryString.method = GET
                addQueryString.exclude = id, cHash, tx_kesearch_pi1[sword]
                forceAbsoluteUrl = 1
                returnLast = url
            }
            wrap = <link rel="canonical" href="|">
        }
        99 = TEXT
        99.value(
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
            <meta name="msapplication-tap-highlight" content="no">
            <link rel="apple-touch-icon" sizes="57x57" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="typo3conf/ext/site_package/Resources/Public/favicons/apple-touch-icon-180x180.png">
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
            <meta name="apple-mobile-web-app-title" content="gulp-frontend-boilerplate">
            <link rel="icon" type="image/png" sizes="228x228" href="typo3conf/ext/site_package/Resources/Public/favicons/coast-228x228.png">
            <link rel="manifest" href="typo3conf/ext/site_package/Resources/Public/favicons/manifest.json">
            <meta name="mobile-web-app-capable" content="yes">
            <meta name="theme-color" content="#020307">
            <meta name="application-name" content="gulp-frontend-boilerplate">
            <link rel="yandex-tableau-widget" href="typo3conf/ext/site_package/Resources/Public/favicons/yandex-browser-manifest.json">
            <meta name="msapplication-TileColor" content="#020307">
            <meta name="msapplication-TileImage" content="typo3conf/ext/site_package/Resources/Public/favicons/mstile-144x144.png">
            <meta name="msapplication-config" content="typo3conf/ext/site_package/Resources/Public/favicons/browserconfig.xml">
            <link rel="icon" type="image/png" sizes="32x32" href="typo3conf/ext/site_package/Resources/Public/favicons/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="192x192" href="typo3conf/ext/site_package/Resources/Public/favicons/android-chrome-192x192.png">
            <link rel="icon" type="image/png" sizes="16x16" href="typo3conf/ext/site_package/Resources/Public/favicons/favicon-16x16.png">
        )
    }
    meta {
        X-UA-Compatible = IE=edge
        X-UA-Compatible.attribute = http-equiv
        viewport = width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no
        msapplication-tap-highlight = no
        format-detection = telephone=no
        charset = utf-8
    }
}