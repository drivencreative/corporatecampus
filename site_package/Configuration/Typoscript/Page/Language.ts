# Sprachkonfiguration
config.linkVars = L(0-1)
config.uniqueLinkVars = 1

# Standardsprache setzen
# auf deutsch setzen
config {
    sys_language_uid = 0
    locale_all = de_DE.utf-8
    language = de
    htmlTag_langKey = de
    meta.language = de
    htmlTag_dir = ltr
    #sys_language_fallBackOrder = 1,0
    #sys_language_mode = strict
    sys_language_overlay = 1
    sys_language_content = 1
}

/*
###########################################################
## Englischer Seitenbaum
#
[globalVar = GP:L = 1]
config {
    sys_language_uid = 1
    locale_all = en_GB.utf-8
    language = en
    htmlTag_langKey = en
    meta.language = en
    htmlTag_dir = ltr
 #  sys_language_overlay = 1
 #  sys_language_content = 1
    sys_language_mode=content_fallback;0
    sys_language_fallBackOrder = 0
}
[global]
*/