plugin.tx_felogin_pi1.templateFile = EXT:site_package/Resources/Private/Templates/ext/felogin/Resources/Private/Templates/MainFrontendLogin.html
    [globalVar = TSFE:id = {$site_package.page.loginPageFormPid}]
plugin.tx_felogin_pi1.templateFile = EXT:site_package/Resources/Private/Templates/ext/felogin/Resources/Private/Templates/LoginPageForm.html
    [global]
plugin.tx_felogin_pi1 {
    welcomeHeader_stdWrap.wrap = <h2>|</h2>
    successHeader_stdWrap.wrap = <h2>|</h2>
    logoutHeader_stdWrap.wrap = <h2>|</h2>
    errorHeader_stdWrap.wrap = <h2>|</h2>
    forgotHeader_stdWrap.wrap = <h2>|</h2>
    storagePid = {$site_package.page.usersPid}
    showForgotPasswordLink = 1
    redirectMode = login
    redirectPageLogin = 1
    showLogoutFormAfterLogin = 0
    redirectPageLoginError = {$site_package.page.loginPageFormPid}
    redirectPageLogout = {$site_package.page.loginPageFormPid}
    forgotMessage_stdWrap.wrap = <p>|</p>
    preserveGETvars = all
    linkConfig {
        parameter = {$site_package.page.loginPageFormPid}
    }
    _LOCAL_LANG.default {
        reset_password = Passwort zurücksetzen
        ll_logout_header = Login
        ll_logout_message = Im Teilnehmerbereich für die Vorstände und Bereichsleiter der DZ BANK Gruppe haben wir Ihnen vertiefendes Material zu den Angeboten des Corporate Campus für Management & Strategie bereitgestellt. Zum Login nutzen Sie bitte Ihre E-Mail-Adresse und Ihr persönliches Passwort.
        ll_enter_your_data = Benutzername oder E-Mail-Adresse
        ll_forgot_reset_message = Bitte geben Sie Ihren Nutzernamen oder Ihre E-Mail-Adresse ein. Sie erhalten anschließend einen Link zum Zurücksetzen des Passwortes.
        ll_forgot_header = Passwort vergessen?
        ll_error_message = Bei der Anmeldung ist ein Fehler aufgetreten. Bitte überprüfen Sie Ihren Nutzernamen und Ihr Passwort und versuchen Sie es erneut. Bleibt der Fehler bestehen, wenden Sie sich bitte an Anita Seubert
        ll_error_header = Anmeldung fehlgeschlagen
        ll_welcome_header = Login
        login = Absenden
        logout = Ausloggen
        password = Passwort
        username = Benutzer
        reset_password = Passwort zurücksetzen
        ll_welcome_message = Bitte geben Sie ihren Nutzername und ihr Passwort ein.
        ll_forgot_reset_message_emailSent = Wir senden Ihnen umgehend einen Link zu, bei dem Sie Ihr Passwort zurücksetzten können.
    }
    _LOCAL_LANG.de {
        ll_forgot_reset_message_emailSent = Wir senden Ihnen umgehend einen Link zu, bei dem Sie Ihr Passwort zurücksetzten können.
    }
}
styles.content.loginform {
    dateFormat = D.M.Y H:i
}